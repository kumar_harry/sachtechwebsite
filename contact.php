<?php include("header.php"); ?>
<!----------------------------------------------------------------------------------------->
<!-- portfolio header start here -->
<!----------------------------------------------------------------------------------------->
<style>
    .map{
        width:100%;
        height:431px;
    }
    .addressHead2{
        font-size: 19px;
        margin-top: 22px;
    }
    .contactsocialIcon a i{
        margin-left:15px;
    }
    .redHeader{
        background: #a3242f none repeat scroll 0 0;
        color: white;
        font-size: 22px;
        padding: 29px;
        text-align: center;
    }
    .contact-button
    {
        padding: 10px;
        width: 200px;
        margin-top:30px;
        background: #ab1522;
    }
    .banner{
        background: url("images/slider/contactpagebanner.jpg");
    }
    .contacthead{
        margin: 50px 50px 0;
        font-size:22px;
        color:white;
    }
    .formbox{
        background: rgba(255, 255, 255, 0.5) none repeat scroll 0 0;
        border-radius: 5px;
        box-shadow: 5px 3px 15px #000;
        margin-bottom: 77px;
        padding: 20px;
        margin-top:25px;
    }
    .labelform{
        margin-bottom:20px;
        background:#ab1522;
    }
    .clear{
        clear:both;
    }
    .bottomline {
        border: 2px solid #ab1522;
        margin-left: 68px;
        width: 70px;
    }
    .contact-address-box{
        border:1px solid #ccc;
        border-bottom:none;
    }
    .contact-address-follow{
        border-bottom: 1px solid #ccc;
    }
</style>
<div class="loader">
    <img src="images/159.gif" class="loader-img" />
</div>
<div class="col-md-12 nogutter banner">
    <p class="contacthead">Contact Us</p>
    <p class="bottomline"></p>
    <div class="col-md-1"></div>
    <div class="col-md-10 formbox">
            <label class="label label-danger labelform">Please Fill All The Required Fields</label><br><br>
            <div class="clear"></div>
            <div class="col-md-3 form-group">
                <label>Name</label>
                <input type="text" class="form-control contact-field contact-name only-letters" placeholder="Enter Name" />
            </div>
            <div class="col-md-3 form-group">
                <label>E-Mail</label>
                <input type="email" class="form-control contact-field contact-email" placeholder="Enter E-Mail Address" />
            </div>
            <div class="col-md-3 form-group">
                <label>Contact Number</label>
                <input type="text" class="form-control contact-field contact-number only-numbers" placeholder="Enter Contact Number" />
            </div>
            <div class="col-md-3 form-group">
                <label>Message</label>
                <input type="text" class="form-control contact-field contact-message" placeholder="Enter Message" />
            </div>
            <div class="clear"></div>
            <div class="col-md-12 text-center">
                <input type="button" class="btn btn-danger contact-button contact-Us" value="Send Message">
            </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="col-md-12 nogutter">
    <p class="redHeader">OUR GLOBAL PRESENCE</p>
</div>
<div class="col-md-12 nogutter" style="margin-bottom:10px">
    <div class="col-md-4">
        <div class="col-md-12 contact-address-box" >
            <p class="addressHead2">Phone</p>
            <div >
                <p class="pull-left">+91-7087972568 <br>+91-7087425488</p><i class="fa fa-phone fa-2x pull-right"></i>
            </div>
        </div>
        <div class="col-md-12 contact-address-box">
            <p class="addressHead2">Address ( India )</p>
            <div >
                <p class="pull-left">#E-110 Industrial Area, Phase7, <br>SAS Nagar Mohali (160055)</p>
                <i class="fa fa-map-marker fa-2x pull-right"></i>
            </div>
        </div>
        <div class="col-md-12 contact-address-box">
            <p class="addressHead2">Address ( Canada )</p>
            <div >
                <p class="pull-left">#329, Calgary street, London, <br>ontario. N5W4X3</p><i class="fa fa-map-marker fa-2x pull-right"></i>
            </div>
        </div>
        <div class="col-md-12 contact-address-box contact-address-follow">
            <p class="addressHead2">Follow Us.</p>
            <div class="contactsocialIcon nogutter">
                <a href="https://www.facebook.com/search/top/?q=SachTech%20Solution%20Private%20Limited" target="_blank">
                    <i title="Facebook" class="fa fa-facebook" style="color: #3C5A98;margin-left:0"></i>
                </a>
                <a href="https://twitter.com/sach_tech"  target="_blank">
                    <i title="Twitter" class="fa fa-twitter" style="color: #32CCFE"></i>
                </a>
                <a href="https://plus.google.com/108110002229769330623"  target="_blank">
                    <i title="Google Plus" class="fa fa-google-plus" style="color: #DC4937"></i>
                </a>
                <a href="https://www.linkedin.com/in/sachtech-solution-private-limited-ab5b5bb7/" target="_blank">
                    <i title="LinkedIn" class="fa fa-linkedin " style="color: #0077B5"></i>
                </a>
                <a href="https://www.youtube.com/stsmentor"  target="_blank">
                    <i title="YouTube" class="fa fa-youtube" style="color: #E52C27  "></i>
                </a>
                <a href="https://www.instagram.com/sachtech/"  target="_blank">
                    <i title="Instagram" class="fa fa-instagram " style="color: #D6005C"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-8 nogutter contact_map_box" >
        <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4992.8260975827!2d76.69890092468904!3d30.719641903193487!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb713d3a3e26a89c1!2sSachTech+Solution+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1501505872203" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
<div class="clear"></div>
<!----------------------------------------------------------------------------------------->
<!-- portfolio header end here -->
<!----------------------------------------------------------------------------------------->
<?php include("footer.php"); ?>

