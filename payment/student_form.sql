-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2017 at 12:09 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company_sachtech`
--

-- --------------------------------------------------------

--
-- Table structure for table `student_form`
--

CREATE TABLE `student_form` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `courses` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zip` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_form`
--

INSERT INTO `student_form` (`id`, `firstname`, `lastname`, `courses`, `city`, `address`, `state`, `country`, `zip`, `phone`, `email`, `amount`) VALUES
(1, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(2, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(3, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(4, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(5, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(6, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(7, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(8, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(9, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(10, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(11, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(12, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(13, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(14, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(15, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(16, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(17, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(18, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(19, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(20, 'test', 'test', 'JAVA', 'test', 'testing m,ndf,mn,m', 'test', 'test', 166512, '963258741', 'test@gmail.com', 12000),
(21, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(22, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(23, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(24, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(25, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(26, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(27, 'test1', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10),
(28, 'test1', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10),
(29, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(30, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10),
(31, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(32, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(33, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(34, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(35, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(36, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(37, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(38, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(39, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(40, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(41, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(42, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(43, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(44, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000),
(45, 'test', 'test', 'JAVA', 'test', 'test', 'test', 'test', 144000, '9417836982', 'test@gmail.com', 10000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student_form`
--
ALTER TABLE `student_form`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student_form`
--
ALTER TABLE `student_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
