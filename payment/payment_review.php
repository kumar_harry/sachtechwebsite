<?php
session_start();
error_reporting(0);
include_once 'header.php';
include_once '../webservice/Constants/DbConfig.php';
include_once '../webservice/Classes/CONNECT.php';
$link = null;
$link = new \Classes\CONNECT();
?>
<head xmlns="http://www.w3.org/1999/html">
  <style>
      ul{
          list-style-type: none;
      }
  </style>
</head>
<div class="container">
    <div class="row" style="margin:40px 0">
        <div class="col-md-8 form-container">
            <?php
            $id = $_SESSION['sach_userId'];
            $linkk = $link->connect();
            if($id == ""){
                header("Location:payment/index.php");
            }
            if(!$linkk)
            {
                header("Location:payment/index.php");
            }
            else {
            $query = "select * from student_form where id= '$id' ";
            $result = mysqli_query($linkk, $query);
            if($result) {
                    $num = mysqli_num_rows($result);
                    if($num > 0){
                        $row = mysqli_fetch_array($result);
                            ?>
                                <div class="col-md-12 form-header text-center" >
                                    <label class="form-text">Dear Sir/ Mam, you are applying for the Course <?php echo $row['courses']; ?>
                                    </label>
                                </div>
                                <div class="col-md-12 text-center">
                            <?php
                            echo "<ul ><li>Course applied for " . $row['courses'] . "</li>";
                            echo "<li>" . $row['amount'] . "</li>
                            <li>" . $row['firstname'] . "</li>
                            <li>" . $row['city'] . "</li>
                            <li" . $row['phone'] . "</li>
                            <li>" . $row['email'] . "</li></ul>";
                            echo "<form method=POST name=customerData action=ccavRequestHandler.php>";
                            echo "<input type=hidden name=id value=".$row['id']." >";
                            echo "<input type=hidden name='firstname' value=".$row['firstname'].">";

                            echo "<input type='hidden' name='city' value=".$row['city'].">";
                            echo "<input type='hidden' name='courses' value=".$row['courses'].">";
                            echo "<input type='hidden' name='parameter' value='123'>";
                            echo "<input type='hidden' name='tid' id='tid' readonly >";
                            echo "<input type='hidden' name='merchant_id' value='141634'>";
                            echo "<input type='hidden' name='amount' value=".$row['amount'].">";
                            echo "<input type='hidden' name='order_id' value=".$row['courses']."_".rand(10,10000)." >";

                            echo "<input type='hidden' name='currency' value='INR'>";
                            echo "<input type='hidden' name='redirect_url' value='http://sachtechsolution.com/payment/ccavResponseHandler.php'>";
                            echo "<input type='hidden' name='cancel_url' value='http://sachtechsolution.com/payment/ccavResponseHandler.php'>";
                            echo "<input type='hidden' name='language' value='EN'>";
                            echo "<input type='hidden' name='billing_name' value='Charli'>";
                            echo "<input type='hidden' name='billing_address' value=".$row['address'].">";
                            echo "<input type='hidden' name='billing_city' value=".$row['city'].">";
                            echo "<input type='hidden' name='billing_state' value=".$row['state'].">";
                            echo "<input type='hidden' name='billing_zip' value=".$row['zip'].">";
                            echo "<input type='hidden' name='billing_country' value=".$row['country'].">";
                            echo "<input type='hidden' name='billing_tel' value=".$row['phone'].">";
                            echo "<input type='hidden' name='billing_email' value=".$row['email'].">";
                            echo "<input type='submit' value='payment' class='btn btn-info'>";
                            echo "</form>";
                    }
                    else
                    {
                        header("Location:payment/index.php");
                    }
                }
                else
                {
                    echo $link->sqlError();
                }
            ?>

                <?php
            }

                ?>


            </div>
        </div>
        <div class="col-md-4 form-container">
            <div class="col-md-12 " style="margin-top: 11%">
                <label >Contact Information</label>
            </div>
            <div class="col-md-12">
                <p>E-110 Industrial Area, Phase 7 Mohali, India
                    +91-7087425488
                    +91-7087972568
                    info@sachtechsolution.com
                    http://sachtechsolution.com</p>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
<script type="text/javascript" src="js/json.js"></script>
<script type="text/javascript">
    $(function(){
        var jsonData;
        var access_code="";
        // shared by CCAVENUE
        var amount="6000.00";
        var currency="INR";

        $.ajax({
            url:'https://secure.ccavenue.com/transaction/transaction.do?command=getJsonData&access_code='+access_code+'&currency='+currency+'&amount='+amount,
            dataType: 'jsonp',
            jsonp: false,
            jsonpCallback: 'processData',
            success: function (data) {
                jsonData = data;
                // processData method for reference
                processData(data);
                // get Promotion details
                $.each(jsonData, function(index,value) {
                    if(value.Promotions != undefined  && value.Promotions !=null){
                        var promotionsArray = $.parseJSON(value.Promotions);
                        $.each(promotionsArray, function() {
                            console.log(this['promoId'] +" "+this['promoCardName']);
                            var	promotions=	"<option value="+this['promoId']+">"
                                +this['promoName']+" - "+this['promoPayOptTypeDesc']+"-"+this['promoCardName']+" - "+currency+" "+this['discountValue']+"  "+this['promoType']+"</option>";
                            $("#promo_code").find("option:last").after(promotions);
                        });
                    }
                });
            },
            error: function(xhr, textStatus, errorThrown) {
                alert('An error occurred! ' + ( errorThrown ? errorThrown :xhr.status ));
                //console.log("Error occured");
            }
        });
        function processData(data){
            var paymentOptions = [];
            var creditCards = [];
            var debitCards = [];
            var netBanks = [];
            var cashCards = [];
            var mobilePayments=[];
            $.each(data, function() {
                // this.error shows if any error
                console.log(this.error);
                paymentOptions.push(this.payOpt);
                switch(this.payOpt){
                    case 'OPTCRDC':
                        var jsonData = this.OPTCRDC;
                        var obj = $.parseJSON(jsonData);
                        $.each(obj, function() {
                            creditCards.push(this['cardName']);
                        });
                        break;
                    case 'OPTDBCRD':
                        var jsonData = this.OPTDBCRD;
                        var obj = $.parseJSON(jsonData);
                        $.each(obj, function() {
                            debitCards.push(this['cardName']);
                        });
                        break;
                    case 'OPTNBK':
                        var jsonData = this.OPTNBK;
                        var obj = $.parseJSON(jsonData);
                        $.each(obj, function() {
                            netBanks.push(this['cardName']);
                        });
                        break;

                    case 'OPTCASHC':
                        var jsonData = this.OPTCASHC;
                        var obj =  $.parseJSON(jsonData);
                        $.each(obj, function() {
                            cashCards.push(this['cardName']);
                        });
                        break;

                    case 'OPTMOBP':
                        var jsonData = this.OPTMOBP;
                        var obj =  $.parseJSON(jsonData);
                        $.each(obj, function() {
                            mobilePayments.push(this['cardName']);
                        });
                }

            });
        }
    });
</script>
