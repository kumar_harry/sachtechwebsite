<html>
<head>
    <title>Best Mobile App Development Company in Mohali India</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name='keywords'
          content='leading mobile app development company in India, best website design company in India, web design services India, website design and development company, Mobile app development company in Mohali, Best Training in Mohali, SachTech Solution'/>
    <meta name='description' content='SachTech Solution is a leading development company offering excellent quality, result oriented solutions. '/>
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/font-awesome.css"/>
    <link rel="stylesheet" href="css/styles.css"/>
    <link rel="stylesheet" href="../css/common.css"/>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/common.js"></script>
    <script type='text/javascript' src='../js/location.js'></script>

</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-3136 node-type-page menu-active-0"
      onload="loadmenu()">
<!--right side bar-->
<div class="rgt_fixed">
    <a href="../request_purposal" class="crpt_film">
        <i class="req_quote "></i>
        <div class="cptext medium">QUOTE</div>
    </a>
    <a href="../contact" class="crpt_film second">
        <i class="sprite qenqry"></i>
        <div class="cptext medium">Quick Enquiry</div>
    </a>
    <a href="../portfolio.php?ios" class="crpt_film four_icon">
        <i class=" ios sprite_social"></i>
        <div class="cptext medium"><span class="intoSmall">i</span>OS</div>
    </a>
    <a href="../portfolio.php?android" class="crpt_film four_icon">
        <i class=" android sprite_social"></i>
        <div class="cptext medium">Android</div>
    </a>
    <a href="../faq" class="crpt_film four_icon">
        <i class="quote sprite_quote"></i>
        <div class="cptext medium">FAQ</div>
    </a>

    <a href="../index.php" class="crpt_film four_icon">
        <i class="quote sprite_quote"></i>
        <div class="cptext medium">PAYMENT</div>
    </a>


</div>
<!--right side bar-->
<header>
    <div class="col-md-12 headerbox nogutter">
        <div class="col-xs-9  col-md-2 nogutter logo_box">
            <a href="../index">
                <img src="../images/logo.png" class="img-responsive logo"/>
            </a>
        </div>
        <div class="col-xs-3 col-md-1 pull-right right_nav_bars">
            <!-- for md device nav-bar start here -->
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <!-- for md device nav-bar end here -->
            <!-- for less md device nav-bar start here here -->
            <div id="nav-icon2">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <!-- for less md device nav-bar end here here -->
        </div>
        <div class="col-xs-12 col-md-2 nogutter social_icons" id="sicons" style="display:none">
            <div class="row social-icons-row">
                <a title="Instagram" href="https://www.instagram.com/sachtech/" target="_blank">
                    <i class="fa fa-instagram social" style="color: #D6005C"></i>
                </a>
                <a href="https://www.youtube.com/stsmentor" data-toggle="tooltip" data-placement="top" target="_blank">
                    <i title="YouTube" class="fa fa-youtube social" style="color: #E52C27"></i>
                </a>
                <a href="https://www.linkedin.com/in/sachtech-solution-private-limited-ab5b5bb7/" target="_blank">
                    <i title="LinkedIn" class="fa fa-linkedin social" style="color: #0077B5"></i>
                </a>
               <!-- <a href="https://stsmentor7@gmail.com" target="_blank">
                    <i title="Skype" class="fa fa-skype social" style="color: #00AFF0"></i>
                </a>-->
                <a href="https://plus.google.com/108110002229769330623" target="_blank">
                    <i title="Google Plus" class="fa fa-google-plus social" style="color: #DC4937"></i>
                </a>
                <a href="https://twitter.com/sach_tech" target="_blank">
                    <i title="Twitter" class="fa fa-twitter social" style="color: #32CCFE"></i>
                </a>
                <a href="https://www.facebook.com/search/top/?q=SachTech%20Solution%20Private%20Limited"
                   target="_blank">
                    <i title="Facebook" class="fa fa-facebook social" style="color: #3C5A98"></i>
                </a>
            </div>
        </div>

        <div class="col-md-7 nogutter main-menu" id="main-menu" style="display:none">
            <div class="row main-menu-large">
                <ul class="menubox">
                    <a href="../index" class="header-ancor">
                        <li class="menuitem">Home</li>
                    </a>
                    <a href="../services" class="header-ancor">
                        <li class="menuitem">Services
                            <div class="sub_menu_box">
                                <ul class="sub_menu">
                                    <a href="../services?service=service-android">
                                        <li class="sub_menu_item header-services" id="header-service-android">
                                            ANDROID APP DEVELOPMENT
                                        </li>
                                    </a>
                                    <a href="../services?service=service-ios">
                                        <li class="sub_menu_item header-services" id="header-service-ios">
                                            <span class="intoSmall">i</span>OS APP DEVELOPMENT
                                        </li>
                                    </a>
                                    <a href="../services?service=service-windowapp">
                                        <li class="sub_menu_item header-services" id="header-service-windowapp">
                                            WINDOW APP DEVELOPMENT
                                        </li>
                                    </a>

                                    <a href="../services?service=service-appmaintainance">
                                        <li class="sub_menu_item header-services" id="header-service-appmaintainance">
                                            APP MAINTENANCE
                                        </li>
                                    </a>
                                    <a href="services?service=service-phpdevelopment">
                                        <li class="sub_menu_item header-services" id="header-service-phpdevelopment">
                                            PHP DEVELOPMENT
                                        </li>
                                    </a>

                                    <a href="../services?service=service-javadevelopment">
                                        <li class="sub_menu_item header-services" id="header-service-javadevelopment">
                                            JAVA DEVELOPMENT
                                        </li>
                                    </a>

                                </ul>
                                <ul class="sub_menu2">


                                    <a href="../services?service=service-ecommerce">
                                        <li class="sub_menu_item header-services" id="header-service-ecommerce">
                                            E-COMMERCE DEVELOPMENT
                                        </li>
                                    </a>
                                    <a href="../services?service=service-cms">
                                        <li class="sub_menu_item header-cms" id="header-service-cms">
                                            CMS DEVELOPMENT
                                        </li>
                                    </a>

                                    <a href="../services?service=service-websitemaintainance">
                                        <li class="sub_menu_item header-services"
                                            id="header-service-websitemaintainance">
                                            WEBSITE MAINTENANCE
                                        </li>
                                    </a>

                                    <a href="../services?service=service-webdesigning">
                                        <li class="sub_menu_item header-services" id="header-service-webdesigning">
                                            WEB DESIGNING SERVICES
                                        </li>
                                    </a>
                                    <a href="../services?service=service-opengl">
                                        <li class="sub_menu_item header-services" id="header-service-opengl">
                                            OPEN GL
                                        </li>
                                    </a>
                                    <a href="../services?service=service-logo">
                                        <li class="sub_menu_item header-services" id="header-service-logo">
                                            LOGO DESIGN
                                        </li>
                                    </a>
                                </ul>
                                <ul class="sub_menu3">
                                    <a href="../services?service=service-flash3d">
                                        <li class="sub_menu_item header-services" id="header-service-flash3d">
                                            FLASH AND 3D ANIMATION
                                        </li>
                                    </a>
                                    <a href="../services?service=service-psdtoxhtml">
                                        <li class="sub_menu_item header-services" id="header-service-psdtoxhtml">
                                            PSD TO XHTML CONVERSION
                                        </li>
                                    </a>
                                    <a href="../services?service=service-seo">
                                        <li class="sub_menu_item header-services" id="header-service-seo">
                                            SEO
                                        </li>
                                    </a>
                                    <a href="services?service=service-emailmarketing">
                                        <li class="sub_menu_item header-services" id="header-service-emailmarketing">
                                            EMAIL MARKETING
                                        </li>
                                    </a>
                                    <a href="../services?service=service-softwaremaintainance">
                                        <li class="sub_menu_item header-services"
                                            id="header-service-softwaremaintainance">
                                            SOFTWARE MAINTENANCE
                                        </li>
                                    </a>
                                    <a href="../services?service=service-outsourcing">
                                        <li class="sub_menu_item header-services" id="header-service-outsourcing">
                                            OUR OUTSOURCING
                                        </li>
                                    </a>

                                </ul>
                            </div>
                        </li>
                    </a>
                    <a href="../portfolio" class="header-ancor">
                        <li class="menuitem">Portfolio</li>
                    </a>
                    <a href="../about" class="header-ancor">
                        <li class="menuitem">About Us
                            <div class="sub_menu_box">
                                <ul class="sub_menu4">
                                    <a href="../about?foundation" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            FOUNDATION
                                        </li>
                                    </a>

                                    <a href="../about?mission" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            MISSION
                                        </li>
                                    </a>

                                    <a href="../about?vision" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            VISION
                                        </li>
                                    </a>

                                    <a href="../about?value" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            VALUE
                                        </li>
                                    </a>
                                </ul>
                            </div>
                        </li>
                    </a>
                    <a href="../skill" class="header-ancor">
                        <li class="menuitem">Skill Development
                            <div class="sub_menu_box">
                                <ul class="sub_menu5">
                                    <a href="../skill?section-motive" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            MOTIVE
                                        </li>
                                    </a>
                                    <a href="../skill?section-need" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            NEED
                                        </li>
                                    </a>
                                    <a href="../skill?section-stand-out" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            HOW WE STAND OUT FROM OTHERS?
                                        </li>
                                    </a>
                                    <a href="../skill?section-program-offer" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            HOW DOES THE PROGRAMS WE OFFER ARE GOOD FOR YOU?
                                        </li>
                                    </a>
                                    <a href="../faq?qry=faq-online-reg" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            ONLINE REGISTRATION
                                        </li>
                                    </a>
                                    <a href="../skill?section-student-testimonial" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            STUDENT TESTIMONIAL
                                        </li>
                                    </a>
                                    <a href="../skill?section-IOT" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            IOT (INTERNET OF THINGS)
                                        </li>
                                    </a>
                                    <a href="../skill?section-ios" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            iOS TRAINING
                                        </li>
                                    </a>
                                </ul>
                                <ul class="sub_menu6">
                                    <a href="../skill?section-java" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            JAVA TRAINING
                                        </li>
                                    </a>
                                    <a href="../skill?section-advance-java" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            ADVANCE JAVA TRAINING
                                        </li>
                                    </a>
                                    <a href="../skill?section-php" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            PHP TRAINING
                                        </li>
                                    </a>
                                    <a href="../skill?section-android" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            ANDROID TRAINING
                                        </li>
                                    </a>
                                    <a href="../skill?section-networking" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            NETWORKING TRAINING
                                        </li>
                                    </a>
                                    <a href="../skill?section-cloud-computing" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            CLOUD COMPUTING
                                        </li>
                                    </a>
                                    <a href="../skill?section-hr" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            HR TRAINING
                                        </li>
                                    </a>
                                    <a href="../skill?section-digital-marketing" class="header_ancor_menu">
                                        <li class="sub_menu_item">
                                            DIGITAL MARKETING
                                        </li>
                                    </a>
                                </ul>
                            </div>
                        </li>
                    </a>
                    <a href="../career" class="header-ancor">
                        <li class="menuitem">Career
                            <div class="sub_menu_box">
                                <ul class="sub_menu4">
                                    <a href="career">
                                        <li class="sub_menu_item">
                                            JOB OPENING
                                        </li>
                                    </a>
                                    <a href="../career?scrl=career-news-room">
                                        <li class="sub_menu_item">
                                            NEWS ROOM
                                        </li>
                                    </a>
                                    <a href="../career?career=workshop">
                                        <li class="sub_menu_item">
                                            WORKSHOP AT COLLEGES
                                        </li>
                                    </a>
                                    <a href="contact">
                                        <li class="sub_menu_item">
                                            JOIN US
                                        </li>
                                    </a>
                                </ul>
                            </div>
                        </li>
                    </a>
                    <a href="../contact" class="header-ancor">
                        <li class="menuitem">Contact Us</li>
                    </a>
                </ul>
            </div>
            <div class="row main-menu-small">
                <ul class="small-menu-ul small-menu-ul-active">
                    <a class="small-menu-ancor" href="../index">
                        <li class="small-menu-li">
                            HOME
                        </li>
                    </a>
                    <li class="small-menu-li" onclick="small_submenu('services')">
                        <a class="small-menu-ancor" >SERVICES</a>
                        <i class="fa fa-chevron-right small-menu-chright pull-right" aria-hidden="true"></i>
                    </li>
                    <a class="small-menu-ancor" href="../portfolio">
                        <li class="small-menu-li">
                            PORTFOLIO
                        </li>
                    </a>
                    <li class="small-menu-li" onclick="small_submenu('about')">
                        <a class="small-menu-ancor" >ABOUT US</a>
                        <i class="fa fa-chevron-right small-menu-chright pull-right" aria-hidden="true"></i>
                    </li>
                    <li class="small-menu-li" onclick="small_submenu('skill')">
                        <a class="small-menu-ancor" >SKILL DEVELOPMENT</a>
                        <i class="fa fa-chevron-right small-menu-chright pull-right" aria-hidden="true"></i>
                    </li>
                    <li class="small-menu-li" onclick="small_submenu('career')">
                        <a class="small-menu-ancor">CAREER</a>
                        <i class="fa fa-chevron-right small-menu-chright pull-right" aria-hidden="true"></i>
                    </li>
                    <a class="small-menu-ancor" href="contact">
                        <li class="small-menu-li">
                            CONTACT US
                        </li>
                    </a>

                    <a class="small-menu-ancor" href="../faq">
                        <li class="small-menu-li">
                            FAQ
                        </li>
                    </a>
                    <a class="small-menu-ancor" href="../request_purposal">
                        <li class="small-menu-li">
                            QUOTE
                        </li>
                    </a>
                </ul>

                <!-- sub menu list start here -->
                <ul class="small-submenu-ul small_sub_services">
                    <li class="small-submenu-li back-li" onclick="small_menu('services')">
                        <i class="fa fa-chevron-left small-menu-chleft " aria-hidden="true"></i>
                        <a class="small-submenu-ancor small-submenu-ancor" href="#">Back</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor header_submenu_ancor" href="../services?service=service-android">ANDROID
                            APP DEVELOPMENT</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-ios">iOS APP DEVELOPMENT</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-windowapp">WINDOW APP
                            DEVELOPMENT</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-appmaintainance">APP
                            MAINTENANCE</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-phpdevelopment">PHP
                            DEVELOPMENT</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-javadevelopment">JAVA
                            DEVELOPMENT</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-ecommerce">E-COMMERCE
                            DEVELOPMENT</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-cms">CMS DEVELOPMENT</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-websitemaintainance">WEBSITE
                            MAINTENANCE</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-webdesigning">WEB DESIGNING
                            SERVICES</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-opengl">OPEN GL</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-logo"> LOGO DESIGN</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-flash3d"> FLASH AND 3D
                            ANIMATION</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-psdtoxhtml">PSD TO XHTML
                            CONVERSION</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-seo">SEO</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-emailmarketing">EMAIL
                            MARKETING</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-softwaremaintainance">SOFTWARE
                            MAINTENANCE</a>
                    </li>
                    <li class="small-submenu-li">
                        <a class="small-submenu-ancor" href="../services?service=service-outsourcing">OUR OUTSOURCING</a>
                    </li>
                </ul>

                <ul class="small-submenu-ul small_sub_skill">
                    <a class="small-submenu-ancor small-submenu-ancor" href="#">
                        <li class="small-submenu-li back-li" onclick="small_menu('skill')">
                            <i class="fa fa-chevron-left small-menu-chleft " aria-hidden="true"></i>
                            Back
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-motive">
                        <li class="small-submenu-li">
                            MOTIVE
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-need">
                        <li class="small-submenu-li">
                            NEED
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-stand-out">
                        <li class="small-submenu-li">
                            HOW WE STAND OUT FROM OTHERS?
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-program-offer">
                        <li class="small-submenu-li">
                            HOW DOES THE PROGRAMS WE OFFER ARE GOOD FOR YOU?
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../faq?qry=faq-online-reg">
                        <li class="small-submenu-li">
                            ONLINE REGISTRATION
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-student-testimonial">
                        <li class="small-submenu-li">
                            STUDENT TESTIMONIAL
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-IOT">
                        <li class="small-submenu-li">
                            IOT (INTERNET OF THINGS)
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-ios">
                        <li class="small-submenu-li">
                            <span class="intoSmall">i</span>OS TRAINING
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-java">
                        <li class="small-submenu-li">
                            JAVA TRAINING
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-advance-java">
                        <li class="small-submenu-li">
                            ADVANCE JAVA TRAINING
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-android">
                        <li class="small-submenu-li">
                            PHP TRAINING
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-networking">
                        <li class="small-submenu-li">
                            NETWORKING TRAINING
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-cloud-computing">
                        <li class="small-submenu-li">
                            CLOUD COMPUTING
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-hr">
                        <li class="small-submenu-li">
                            HR TRAINING
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../skill?section-digital-marketing">
                        <li class="small-submenu-li">
                            DIGITAL MARKETING
                        </li>
                    </a>
                </ul>

                <ul class="small-submenu-ul small_sub_career">
                    <li class="small-submenu-li back-li" onclick="small_menu('career')">
                        <i class="fa fa-chevron-left small-menu-chleft " aria-hidden="true"></i>
                        <a class="small-submenu-ancor small-submenu-ancor" href="#">Back</a>
                    </li>
                    <a class="small-submenu-ancor" href="../career">
                        <li class="small-submenu-li">
                            JOB OPENING
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../career?scrl=career-news-room">
                        <li class="small-submenu-li">
                            NEWS ROOM
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../career?career=workshop">
                        <li class="small-submenu-li">
                            WORKSHOP AT COLLEGES
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../contact">
                        <li class="small-submenu-li">
                            JOIN US
                        </li>
                    </a>
                </ul>
                <ul class="small-submenu-ul small_sub_about">
                    <li class="small-submenu-li back-li" onclick="small_menu('about')">
                        <i class="fa fa-chevron-left small-menu-chleft " aria-hidden="true"></i>
                        <a class="small-submenu-ancor small-submenu-ancor" href="#">Back</a>
                    </li>
                    <a class="small-submenu-ancor" href="../about?foundation">
                        <li class="small-submenu-li">
                            FOUNDATION
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../about?mission">
                        <li class="small-submenu-li">
                            MISSION
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../about?vision">
                        <li class="small-submenu-li">
                            VISION
                        </li>
                    </a>
                    <a class="small-submenu-ancor" href="../about?value">
                        <li class="small-submenu-li">
                            VALUE
                        </li>
                    </a>
                </ul>
                <!-- sub menu list start here -->


            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="col-md-12 strip"></div>
</header>

<!--modal start here-->
<div id="messageModal" class="modal" style="">
    <!-- Modal content -->
    <div class="modal-dialog modal-mg">
        <div class="modal-content" style="width: 100%;">
            <h4 class="modal-title career_submit_text"></h4>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<!--modal end here-->
<!--modal start here-->
<div id="careerModal" class="modal" style="">
    <!-- Modal content -->
    <div class="modal-dialog modal-mg">
        <div class="modal-content" style="width: 100%;">
            <h4 class="modal-title career_submit_text"></h4>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<!--modal end here-->