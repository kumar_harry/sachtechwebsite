<?php
include_once 'header.php';
?>
<head xmlns="http://www.w3.org/1999/html">
    <script>
        window.onload = function () {
            var d = new Date().getTime();
            document.getElementById("tid").value = d;
    </script>
</head>
<div class="container">
    <div class="row">
            <div class="col-md-8 form-container">
            <div class="col-md-12 form-header">
                <label class="form-text">FILL ALL THE INFORMATION CORRECTLY</label>
            </div>
            <div class="col-md-12 form-content">
                <div class="col-md-6 form-field">
                        <input type="text" class="form-control " placeholder="Enter First Name" id="firstname"
                               name="firstname" />
                </div>
                <div class="col-md-6 form-field">
                    <input type="text" class="form-control" placeholder="Enter Last Name" id="lastname" name="lastname"/>
                </div>
                <div class="col-md-6 form-field">
                    <select id="courses" class="form-control" name="courses">
                        <option value="">Select Course</option>
                        <option value="Java">JAVA</option>
                        <option value="Php">PHP</option>
                        <option value="Android">ANDROID</option>
                        <option value="Networking">NETWORKING</option>
                    </select>
                </div>
                <div class="col-md-6 form-field">
                    <select id="branches" class="form-control">
                        <option value="">Select Branch</option>
                        <option name="mohali">MOHALI</option>
                        <option name="usa">USA</option>
                    </select>
                </div>
                <div class="col-md-6 form-field">
                    <input type="text" class="form-control" placeholder="Enter Country" id="country" name="country" required/>
                </div>
                <div class="col-md-6 form-field">
                    <input type="text" class="form-control" placeholder="Enter State" id="state" name="state" required/>
                </div>
                <div class="col-md-6 form-field">
                    <input type="text" class="form-control" placeholder="Enter Town/City" required id="city" name="city"/>
                </div>
                <div class="col-md-6 form-field">
                    <input type="text" class="form-control" placeholder="Address" required id="address" name="address"/>
                </div>

                <div class="col-md-6 form-field">
                    <input type="text" class="form-control only-numbers" placeholder="Enter Phone Number" id="phone" name="phone" required/>
                </div>
                <div class="col-md-6 form-field">
                    <input type="text" class="form-control" placeholder="Enter Zip Code" id="zip" name="zip" required/>
                </div>
                <div class="col-md-6 form-field">
                    <input type="text" class="form-control" placeholder="Enter Email Address" id="email" name="email" required/>
                </div>
                <div class="col-md-6 form-field">
                    <input type="text" class="form-control only-numbers" placeholder="Enter Amount" name="amount" id="amount"/>
                </div>
                <div class="col-md-12 form-field">
                    <input class="btn btn-primary btn-sm pull-right paymentBtn" type="button" value="Submit">
                </div>
            </div>
        </div>
        <div class="col-md-4 form-container">

            <div class="col-md-12 " style="margin-top: 11%">
                <label>Contact Information</label>
            </div>
            <div class="col-md-12">
                <p>E-110 Industrial Area, Phase 7 Mohali, India
                    +91-7087425488
                    +91-7087972568
                    info@sachtechsolution.com
                    http://sachtechsolution.com</p>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
<script language="javascript" type="text/javascript" src="js/json.js"></script>
<script type="text/javascript">
    $(function () {
        var jsonData;
        var access_code = "" // shared by CCAVENUE
        var amount = "6000.00";
        var currency = "INR";
        $.ajax({
            url: 'https://secure.ccavenue.com/transaction/transaction.do?command=getJsonData&access_code=' + access_code + '&currency=' + currency + '&amount=' + amount,
            dataType: 'jsonp',
            jsonp: false,
            jsonpCallback: 'processData',
            success: function (data) {
                jsonData = data;
                // processData method for reference
                processData(data);
                // get Promotion details
                $.each(jsonData, function (index, value) {
                    if (value.Promotions != undefined && value.Promotions != null) {
                        var promotionsArray = $.parseJSON(value.Promotions);
                        $.each(promotionsArray, function () {
                            console.log(this['promoId'] + " " + this['promoCardName']);
                            var promotions = "<option value=" + this['promoId'] + ">"
                                + this['promoName'] + " - " + this['promoPayOptTypeDesc'] + "-" + this['promoCardName'] + " - " + currency + " " + this['discountValue'] + "  " + this['promoType'] + "</option>";
                            $("#promo_code").find("option:last").after(promotions);
                        });
                    }
                });
            },
            error: function (xhr, textStatus, errorThrown) {
                alert('An error occurred! ' + ( errorThrown ? errorThrown : xhr.status ));
                //console.log("Error occured");
            }
        });
        // Emi section end
        // below code for reference
        function processData(data) {
            var paymentOptions = [];
            var creditCards = [];
            var debitCards = [];
            var netBanks = [];
            var cashCards = [];
            var mobilePayments = [];
            $.each(data, function () {
                // this.error shows if any error
                console.log(this.error);
                paymentOptions.push(this.payOpt);
                switch (this.payOpt) {
                    case 'OPTCRDC':
                        var jsonData = this.OPTCRDC;
                        var obj = $.parseJSON(jsonData);
                        $.each(obj, function () {
                            creditCards.push(this['cardName']);
                        });
                        break;
                    case 'OPTDBCRD':
                        var jsonData = this.OPTDBCRD;
                        var obj = $.parseJSON(jsonData);
                        $.each(obj, function () {
                            debitCards.push(this['cardName']);
                        });
                        break;
                    case 'OPTNBK':
                        var jsonData = this.OPTNBK;
                        var obj = $.parseJSON(jsonData);
                        $.each(obj, function () {
                            netBanks.push(this['cardName']);
                        });
                        break;

                    case 'OPTCASHC':
                        var jsonData = this.OPTCASHC;
                        var obj = $.parseJSON(jsonData);
                        $.each(obj, function () {
                            cashCards.push(this['cardName']);
                        });
                        break;

                    case 'OPTMOBP':
                        var jsonData = this.OPTMOBP;
                        var obj = $.parseJSON(jsonData);
                        $.each(obj, function () {
                            mobilePayments.push(this['cardName']);
                        });
                }
            });
        }
    });
    /*payment form start here*/
    $(".paymentBtn").click(function(){
        var firstname   =   $("#firstname").val();
        var lastname    =   $("#lastname").val();
        var courses     =   $("#courses").val();
        var city        =   $("#city").val();
        var address     =   $("#address").val();
        var state       =   $("#state").val();
        var country     =   $("#country").val();
        var zip         =   $("#zip").val();
        var phone       =   $("#phone").val();
        var email       =   $("#email").val();
        var amount      =   $("#amount").val();
        if(firstname == "" || lastname == "" || courses == "" || city == "" ||  address == "" ||
            state == "" || country  == "" || zip == "" || phone == "" || email == "" ||  amount == "")
        {
            messageModal("All fields are neccessay","red");
            return false;
        }
        if(validateEmail(email)){
            var url = "../webservice/purposal_send.php";
            $.post(url,{"type":"paymentUser","firstname":firstname,"lastname":lastname,"courses":courses,"city":city,
                "address":address,"state":state,"country":country,"zip":zip,"phone":phone,"email":email,"amount":amount},function(data){
                var status = data.Status;
                if(status == "Success"){
                    window.location = "payment_review.php";
                }
                else
                {
                    messageModal(data.Message,"red");
                }
            });
        }
        else
        {
            messageModal("Please enter valid email address","red");
            return false;
        }
    });
    /*common method please use this method start here*/
    $('.only-letters').bind('keyup blur',function(){
        var node = $(this);
        node.val(node.val().replace(/[^a-zA-Z]/g,'') );
    });
    $('.only-numbers').bind('keyup blur',function(){
        var node = $(this);
        node.val(node.val().replace(/[^0-9]/g,'') );
    });


    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    /*payment form end here*/
</script>


