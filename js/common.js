/* $(window).height();
 $(document).height();
var screenHeight    = $(window).width();
var screenWidth     = $(document).width();
alert("screenHeight = "+ screenHeight + " screenWidth = "+screenWidth);*/
var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

$( window ).load(function() {
    //alert("width = "+x + ' Height =  ' + y);
});
/*for fade top header start here*/

/*common method please use this method start here*/
$('.only-letters').bind('keyup blur',function(){
    var node = $(this);
    node.val(node.val().replace(/[^a-zA-Z]/g,'') ); }
);
$('.only-numbers').bind('keyup blur',function(){
    var node = $(this);
    node.val(node.val().replace(/[^0-9]/g,'') ); }
);


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function messageModal(message, color) {
    $("#messageModal").modal("show");
    $(".modal-body").html("<p style='color:" + color + "; font-size: 16px'>" + message + "</p>");
    setTimeout(function () {
        $("#messageModal").modal("hide");
    }, 3000);
}

/*common method please use this method start here*/
function loadmenu() {
    $("#sicons").show("slow");
    $("#main-menu").show("slow");
}

$(document).ready(function () {
    $('#nav-icon1').click(function () {
        $(this).toggleClass('open');
        $(".main-menu").slideToggle(500);
    });
    $('#nav-icon2').click(function () {
        $(this).toggleClass('open');
        $(".main-menu-small").toggle(600);
        $(".small-submenu-ul").hide();
        $(".small-menu-ul").show();
    });
});
/*for fade top header end here*/

/*for small devices header list hide show start here*/
function small_submenu(ulClass) {
    $(".small-menu-ul").hide(500);
    $(".small_sub_" + ulClass).show(500);
}

/*show main menu list start here*/

/*show main menu list end here*/
function small_menu(ulClass) {
    $(".small-menu-ul").show(500);
    $(".small_sub_" + ulClass).hide(500);
}

/*for small devices header list hide show end   here*/







/*--------------------------------------------------------------------------*/
/* About us part start here*/
/*--------------------------------------------------------------------------*/
$(".about-menu-li").click(function () {
    var id = this.id;
    $("#" + id + "-content").show();
    $(".about-menu-li").removeClass("about-menu-li-active");
    $("#" + id).addClass("about-menu-li-active");

    $('html, body').animate({
        scrollTop: $("#" + id + "-content").offset().top
    }, 800);
});
/*--------------------------------------------------------------------------*/
/* About us part end here*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/* portfolio part start here*/
/*--------------------------------------------------------------------------*/
/*portfolio type change start here*/
$(".port-type").click(function () {
    var port_type_id = this.id;
    switch (port_type_id) {
        case "port-all":
            $(".port-type-text").html("All");
            $("#web-port-design-content").fadeIn(500);
            $("#android-port-design-content").fadeIn(500);
            $("#ios-port-design-content").fadeIn(500);
            $("#logo-port-design-content").fadeIn(500);
            $("#digital-port-design-content").fadeIn(500);
            break;
        case "port-logo-design":
            $(".port-type-text").html("Logo Design");
            $("#web-port-design-content").fadeOut(500);
            $("#android-port-design-content").fadeOut(500);
            $("#ios-port-design-content").fadeOut(500);
            $("#logo-port-design-content").show(500);
            $("#digital-port-design-content").fadeOut(500);
            break;
        case "port-mobile-app":
            $(".port-type-text").html("Mobile Application");
            $("#web-port-design-content").fadeOut(500);
            $("#android-port-design-content").fadeIn(500);
            $("#ios-port-design-content").fadeOut(500);
            $("#logo-port-design-content").fadeOut(500);
            $("#digital-port-design-content").fadeOut(500);
            break;
        case "port-web-develop":
            $(".port-type-text").html("Web Development");
            $("#web-port-design-content").fadeIn(500);
            $("#android-port-design-content").fadeOut(500);
            $("#ios-port-design-content").fadeOut(500);
            $("#logo-port-design-content").fadeOut(500);
            $("#digital-port-design-content").fadeOut(500);
            break;
        case "port-ios":
            $(".port-type-text").html("iOS Application");
            $("#web-port-design-content").fadeOut(500);
            $("#android-port-design-content").fadeOut(500);
            $("#ios-port-design-content").fadeIn(500);
            $("#logo-port-design-content").fadeOut(500);
            $("#digital-port-design-content").fadeOut(500);
            break;
        case "port-digital-mark":
            $(".port-type-text").html("Digital Marketing");
            $("#web-port-design-content").fadeOut(500);
            $("#android-port-design-content").fadeOut(500);
            $("#ios-port-design-content").fadeOut(500);
            $("#logo-port-design-content").fadeOut(500);
            $("#digital-port-design-content").fadeIn(500);
            break;
        default:
            $("#web-port-design-content").fadeIn(500);
            $("#android-port-design-content").fadeIn(500);
            $("#ios-port-design-content").fadeIn(500);
            $("#logo-port-design-content").fadeIn(500);
            $("#digital-port-design-content").fadeIn(500);
            break;
    }
});
/*portfolio type change end  here*/

/*--------------------------------------------------------------------------*/

/* portfolio part end here*/

/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/

/* send purposal  part start here*/

/*--------------------------------------------------------------------------*/
$("#purp-submit").click(function () {
    var client_name         = $("#client_name").val();
    var client_email        = $("#client_email").val();
    var client_contact      = $("#client_contact").val();
    var client_budget       = $("#client_budget").val();
    var client_country      = $("#countryId").val();
    var client_social_id    = $("#client_social_id").val();
    var client_description  = $("#client_description").val();

    if (client_name == "" || client_email == "" || client_contact == "" || client_budget == "" || client_country == "" ||
         client_description == "") {
        messageModal("All fields necessary", "#ab1522");
    }
    else {
        if (validateEmail(client_email)) {
            if(!isNaN(client_contact))
            {
                var url = "webservice/purposal_send.php?type=send_purposal&client_name=" + client_name + "&client_email=" + client_email +
                    "&client_contact=" + client_contact + "&client_budget=" + client_budget +
                    "&client_country=" + client_country + "&client_social_id=" + client_social_id + "&client_description=" + client_description;
                $(".loader").show();
                $.get(url, function (data) {
                    var status = data.Status;
                    var Message = data.Message;
                    if (status == "Success") {
                        $(".loader").hide();

                        messageModal("Thanks for Contacting us. We will get back to you very soon.", "green");
                        $("#client_name").val("");
                        $("#client_email").val("");
                        $("#client_contact").val("");
                        $("#client_budget").val("");
                        $("#client_social_id").val("");
                        $("#client_description").val("");
                    }
                    else {
                        $(".loader").hide();
                        messageModal(Message, "#ab1522");
                    }
                });
            }
            else
            {
                messageModal("Please fill valid mobile number", "#ab1522");
            }
        }
        else {
            messageModal("Please fill valid email address", "#ab1522");
        }
    }
});

/*--------------------------------------------------------------------------*/

/* send purposal  part end here*/

/*--------------------------------------------------------------------------*/
$(".seo-heading-list").click(function () {
    var id = this.id;
    $(".serve-seo-package-list").hide();
    $(".serve-seo-left-image").hide();
    $(".seo-heading-list").removeClass("seo-head-active");
    $("#" + id).addClass("seo-head-active");
    $("." + id + "-content").show();
});
/*--------------------------------------------------------------------------*/

/* service part start here*/

/*--------------------------------------------------------------------------*/
function serviceData() {
    var urlData = window.location.href;
    if(urlData.indexOf("?")>0) {
        urlData = urlData.split("service=");
        /*service type in switch param*/
        switch (urlData[1]) {
            case "service-android":
                $(".android-services-data").show();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").hide();
                $(".seo-services-data").hide();
                break;
            case "serv-refrd-sachtech":
                $(".android-services-data").show();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").hide();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-refrd-sachtech").offset().top
                }, 800);
                break;
                break;
            case "service-ios":
                $(".android-services-data").show();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").hide();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-ios-section").offset().top
                }, 800);
                break;
            case "service-windowapp":
                $(".android-services-data").show();
                $(".web-services-data").hide();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                $(".opengl-services-data").hide();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-windowapp-section").offset().top
                }, 800);
                break;
            case "service-appmaintainance":
                $(".android-services-data").show();
                $(".web-services-data").hide();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                $(".opengl-services-data").hide();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-appmaintainance-section").offset().top
                }, 800);
                break;

            case "service-opengl":
                $(".android-services-data").hide();
                $(".web-services-data").hide();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".opengl-services-data").show();
                $(".seo-services-data").hide();
                break;

            case "service-psdtoxhtml":
                $(".android-services-data").hide();
                $(".web-services-data").hide();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".opengl-services-data").show();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-psdtoxhtml-section").offset().top
                }, 800);
                break;
            case "service-flash3d":
                $(".android-services-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").show();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-flash3d-section").offset().top
                }, 800);
                break;
            case "service-logo":
                $(".android-services-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").show();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-logo-section").offset().top
                }, 800);
                break;

            case "service-softwaremaintainance":
                $(".android-services-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").hide();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".seo-services-data").hide();
                $(".digital-services-data").show();
                $('html, body').animate({
                    scrollTop: $(".serv-swmaintainance-section").offset().top
                }, 800);
                break;
            case "service-emailmarketing":
                $(".android-services-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").hide();
                $(".seo-services-data").hide();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                $(".digital-services-data").show();
                $('html, body').animate({
                    scrollTop: $(".serv-emailmarketing-section").offset().top
                }, 800);
                break;
            case "service-outsourcing":
                $(".android-services-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").hide();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                $(".seo-services-data").hide();
                $(".digital-services-data").show();
                $('html, body').animate({
                    scrollTop: $(".serv-outsourcing-section").offset().top
                }, 800);
                break;


            case "service-seo":
                $(".android-services-data").hide();
                $(".web-services-data").hide();
                $(".opengl-services-data").hide();
                $(".digital-services-data").show();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $('html, body').animate({
                    scrollTop: $(".serv-seo-section").offset().top
                }, 800);
                break;

            case "service-phpdevelopment":
                $(".android-services-data").hide();
                $(".web-services-data").show();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".opengl-services-data").hide();
                $(".seo-services-data").hide();
                break;

            case "service-javadevelopment":
                $(".android-services-data").hide();
                $(".web-services-data").show();
                $(".opengl-services-data").hide();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-java-section").offset().top
                }, 800);
                break;
            case "service-ecommerce":
                $(".android-services-data").hide();
                $(".web-services-data").show();
                $(".opengl-services-data").hide();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-ecommerce-section").offset().top
                }, 800);
                break;
            case "service-webdesigning":
                $(".android-services-data").hide();
                $(".web-services-data").show();
                $(".opengl-services-data").hide();
                $(".seo-services-data").hide();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $('html, body').animate({
                    scrollTop: $(".serv-webdesigning-section").offset().top
                }, 800);
                break;
            case "service-websitemaintainance":
                $(".android-services-data").hide();
                $(".web-services-data").show();
                $(".opengl-services-data").hide();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-webmaintainance-section").offset().top
                }, 800);
                break;
            case "service-cms":
                $(".android-services-data").hide();
                $(".web-services-data").show();
                $(".opengl-services-data").hide();
                $(".android-services-slider-data").hide();
                $(".web-services-slider-data").show();
                $(".seo-services-data").hide();
                $('html, body').animate({
                    scrollTop: $(".serv-cms-section").offset().top
                }, 800);
                break;

            default:
                $(".android-services-data").show();
                $(".android-services-slider-data").show();
                $(".web-services-slider-data").hide();
                break;
        }
    }
    else {
        $(".android-services-data").show();
        $(".web-services-data").hide();
        $(".opengl-services-data").hide();
        $(".seo-services-data").hide();
    }
}

/*--------------------------------------------------------------------------*/

/* service part end here*/

/*--------------------------------------------------------------------------*/

$(function() {
    $('marquee').mouseover(function() {
        $(this).attr('scrollamount',0);
    }).mouseout(function() {
        $(this).attr('scrollamount',5);
    });
});


/*--------------------------------------------------------------------------*/

/* career part start here*/

/*--------------------------------------------------------------------------*/
function careerContent() {
    var urlData = window.location.href;
    urlData = urlData.split("career=");
    urlData = urlData[1];
    switch (urlData) {
        case "news-room":
            $('html, body').animate({
                scrollTop: $(".career-news-room").offset().top
            }, 800);
            break;
        case "workshop":
            $('html, body').animate({
                scrollTop: $(".career-workshop").offset().top
            }, 800);
    }
    $(".career_dev_heading").html("iPhone Developer");
    $(".career_skill").html("Xcode 7.x, AppCode");
    $(".career_number_opening").html("2");
    $(".career_experince").html("1 to 3 Years");
    $(".career_qualification").html("B.Tech/MCA");
}

$(".career-resp-tab-item").click(function () {
    var fieldId = this.id;
    $(".career-resp-tab-item").removeClass("career-resp-tab-active");
    $("#" + fieldId).addClass("career-resp-tab-active");
    switch (fieldId) {
        case "career_ios":
            $(".career_dev_heading").html("iPhone Developer");
            $(".career_skill").html("Xcode 7.x, AppCode");
            $(".career_number_opening").html("2");
            $(".career_experince").html("1 to 3 Years");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        case "career_php":
            $(".career_dev_heading").html("Php Developer");
            $(".career_skill").html("MVC, SQL Server,Angularjs, KnockoutjsJquery, Javascript,CMS");
            $(".career_number_opening").html("1");
            $(".career_experince").html("1 to 3 Years");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        case "career_android":
            $(".career_dev_heading").html("Android Developer");
            $(".career_skill").html("NDK, Worked with multiple 3rd party SDKs, Android studio, MVP, MVC & MVVM");
            $(".career_number_opening").html("0");
            $(".career_experince").html("1.5 to 2 Years");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        case "career_dotnet":
            $(".career_dev_heading").html(".Net Developer");
            $(".career_skill").html("ASP.NET, C#, MVC, SQL Server, WCF, Web APIEntity ramework, Angularjs, KnockoutjsJquery, Javascript");
            $(".career_number_opening").html("0");
            $(".career_experince").html("3 to 5 Years");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        case "career_seo":
            $(".career_dev_heading").html("SEO");
            $(".career_skill").html("Content writing, Grammatical good, having knowledge about SMM,Conversion Rate Optimization - CRO ");
            $(".career_number_opening").html("1");
            $(".career_experince").html("Fresher");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        case "career_graphic_designer":
            $(".career_dev_heading").html("Graphic Designer");
            $(".career_skill").html("Visual Ideation / Creativity, Typography, Layout/Conversion Optimization" +
                ", Print Design, Software(Photoshop, Illustrator, InDesign, Corel Draw)");
            $(".career_number_opening").html("1");
            $(".career_experince").html("3 to 5 Years");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        case "career_digital":
            $(".career_dev_heading").html("Digital Marketing");
            $(".career_skill").html("Good knowledge of Google & Bing webmaster tool, Experience with Googles Adwords Keyword Planner, SEOMoz, ahrefs, Semrush and other SEO tools.");
            $(".career_number_opening").html("0");
            $(".career_experince").html("4 to 5 Years");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        case "career_bussineeAnalyst":
            $(".career_dev_heading").html("Business Analyst");
            $(".career_skill").html("Problem Solving, Excellent Communication Skills, Data analyze, Knowledge about the wire-framing tools");
            $(".career_number_opening").html("2");
            $(".career_experince").html("1 Years");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        case "career_java":
            $(".career_dev_heading").html("Java Developer");
            $(".career_skill").html("<ul class='career_desc_ul'><li>Having knowledge of Spring, Hibernate and JPA, Microservices, Spring Boot.</li>" +
                "<li>Should have worked on Database procedures and all major DBs including MSSQL, MYSQL, Postgres and Oracle.</li>" +
                "<li>Knowledge of NoSQL DB will be good.</li>" +
                "<li>Should have experience working on Webservices in JSON/SOAP.</li>" +
                "<li>Knowledge of design patterns.</li></ul>");
            $(".career_number_opening").html("0");
            $(".career_experince").html("2 to 3 Years");
            $(".career_qualification").html("B.Tech/MCA");
            break;
        default:
            break;
    }
});
$(".career_apply_now").click(function () {
    if($(".career_number_opening").html() <= 0)
    {
        messageModal("Sorry ! no current job opening now.","#ab1522");
    }
    else
    {
        var digit_one = Math.floor(Math.random() * 10) + 99;
        var digit_two = Math.floor(Math.random() * 9) + 3;

        $("#career_apply_modal").modal("show");
        $(".modal-body").html('<div class="form-group"><p class="modal-message"></p></div> <div class="form-group">' +
            '<label for="form-first-name">Name</label><input type="text" placeholder="Name..." onkeyup="validName(this)" class="car_app_name career_fields form-first-name' +
            '  form-control" id="car_app_name" value=""></div><div class="form-group"><label for="form-email">Email</label><input type="email"' +
            ' placeholder="Email..." class="car_app_email form-email form-control" id="form-email"> </div> <div class="form-group">' +
            '<label for="form-phone">Phone</label><input type="text" placeholder="Phone..." class="car_app_phone form-phone form-control"' +
            'id="form-phone" onkeyup="validNumber(this)"></div><div class="form-group"><label for="form-about-yourself">Position applied for</label>' +
            '<input type="text" id="job_type" class="car_app_job_type form-email form-control" readonly="" disabled></div><div class="form-group"><label for="form-phone">Attachment</label><input type="file" class="car_app_file" ' +
            'id="fileUpload" style="border:none;"></div><div class="form-group"><span id="car_app_digit1">' + digit_one + '</span>&nbsp;+&nbsp;' +
            '<span id="car_app_digit2">' + digit_two + '</span><input onkeyup="validNumber(this)" class=" form-control" type="text" id="car_app_answer" placeholder="Enter sum">' +
            '</div><div class="row"> <div class="col-md-6"><div class="get-btn"><input name="submit" onclick="career_apply_mail()" ' +
            'type="submit" class="get-touch-btn addMargin career_applybtn" value="Apply Now"></div></div><div class="col-md-6">' +
            ' <!--<progress id="progressBar" value="0" max="100" style="width:300px;"></progress><h3 id="status"></h3><p id="loaded_n_total"></p>--></div> </div> ' +
            '');
        var job_type = $("#career_job_type").html();
        $("#career_apply_modal #job_type").val(job_type);
    }


});
function validName(valueParam) {
    var node = $(valueParam);
    node.val(node.val().replace(/[^a-z]/g,'') );
}
function validNumber(valueParam) {
    var node = $(valueParam);
    node.val(node.val().replace(/[^0-9]/g, ''));
}
/*for apply career mail send start here*/

function career_apply_mail() {
    var car_app_name = $("#career_apply_modal #car_app_name").val();
    var car_app_email = $("#career_apply_modal .car_app_email").val();
    var car_app_phone = $("#career_apply_modal .car_app_phone").val();
    var car_app_job_type = $("#career_apply_modal .car_app_job_type").val();
    var filename         = $("#career_apply_modal .car_app_file").val();
    var car_app_digit1 = parseInt($("#career_apply_modal #car_app_digit1").html());
    var car_app_digit2 = parseInt($("#career_apply_modal #car_app_digit2").html());
    var car_app_answer = parseInt($("#career_apply_modal #car_app_answer").val());
    var data = new FormData();
    if (car_app_name == "" || car_app_email == "" || car_app_phone == "" || car_app_job_type == "" || filename == "") {
        $(".modal-message").html("All Fields are necessary");
        $(".modal-message").css("color", "#ab1522");
        return false;
    }
    else {
        if (car_app_digit1 + car_app_digit2 != car_app_answer) {
            $(".modal-message").html("Invalid Captcha ! Please try again ");
            $(".modal-message").css("color", "#ab1522");
            var digit_one = Math.floor(Math.random() * 10) + 99;
            var digit_two = Math.floor(Math.random() * 9) + 3;
            $("#career_apply_modal #car_app_digit1").html(digit_one);
            $("#career_apply_modal #car_app_digit2").html(digit_two);
            return false;
        }
        else {
            if (validateEmail(car_app_email)) {
                if (!isNaN(car_app_phone)) {
                    var file_ext = filename.split(".");
                    file_ext = file_ext[file_ext.length - 1];
                    if (file_ext == "pdf" || file_ext == "doc" || file_ext == "docx") {
                        $(".loader").show();
                        // var _file = document.getElementById('fileUpload');
                        var _file = $('#career_apply_modal #fileUpload')[0];
                        data.append('type', "careerApplyMail");
                        data.append('career_attachFile', _file.files[0]);
                        data.append('car_app_name', car_app_name);
                        data.append('car_app_email', car_app_email);
                        data.append('car_app_phone', car_app_phone);
                        data.append('car_app_job_type', car_app_job_type);
                    }
                    else {
                        $(".modal-message").html("File Should Be PDF,DOC and DOCX  Formats Only.");
                        $(".modal-message").css("color", "#ab1522");
                        return false;
                    }
                }
                else {
                    $(".modal-message").html("Please fill valid mobile number.");
                    $(".modal-message").css("color", "#ab1522");
                    return false;
                }
            }
            else {
                $(".modal-message").html("Please fill valid email address.");
                $(".modal-message").css("color", "#ab1522");
                return false;
            }
        }

        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                var response = request.response;
                response = $.parseJSON(response);
                var status = response.Status;
                var Message = response.Message;
                if (status == "Success") {
                    $(".loader").hide();
                    $("#career_apply_modal").modal("hide");
                    messageModal("Thank You ! <br> Your Resume submitted successfully to our Management Team.<br> Our HR contact you as soon as possible!", "green");
                }
                else {
                    $(".loader").hide();
                    messageModal("Something Went Wrong !!! Please Try After Some Time", "#ab1522");
                }
            }
        };
        request.open('POST', 'webservice/purposal_send.php');
        request.send(data);
    }
}
function readURL(input) {
    if (input.files) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var filename = $("#" + input.id).val();
            var file_ext = filename.split(".");
            file_ext = file_ext[file_ext.length - 1];
            if (file_ext == "jpeg" || file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                $("#" + input.id + "upload").attr('src', e.target.result);
            }
            else if (file_ext == "pdf") {
                $("#" + input.id + "upload").attr('src', 'images/pdf.ico');
            }
            else if (file_ext == "doc" || file_ext == "docx") {
                $("#" + input.id + "upload").attr('src', 'images/doc.png');
            }
            else if (file_ext == "txt") {
                $("#" + input.id + "upload").attr('src', 'images/txt.png');
            }
            else {
                alert("File Should Be JPG,PNG,JPEG,GIF,PDF,DOC,DOCX and TXT Formats Only");
                return false;
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

/*for apply career mail send end here*/

/*--------------------------------------------------------------------------*/

/* career part end here*/

/*--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------*/

/* contact us  part start here*/

/*--------------------------------------------------------------------------*/
function successContant(message, color) {
    $("#messageModal").modal("show");
    $(".modal-body").html("<p style='color:" + color + "; font-size: 18px'>" + message + "</p><div class='row contactSuccessRow ' >" +
        "<a href='index.php'><input type='button' class='contactSuccessBtn' value='Ok'></a></div>");
}

$(".contact-Us").click(function () {
    var contact_name = $(".contact-name").val();
    var contact_email = $(".contact-email").val();
    var contact_number = $(".contact-number").val();
    var contact_message = $(".contact-message").val();
    if (contact_name == "" || contact_email == "" || contact_number == " " || contact_message == "") {
        messageModal("All Fields are necessary", "#ab1522");
    }
    else {
        if (validateEmail(contact_email)) {
            if (!isNaN(contact_number)) {
                $(".loader").show();
                var url = "webservice/purposal_send.php?type=contactus&contact_name=" + contact_name + "&contact_email=" + contact_email +
                    "&contact_number=" + contact_number + "&contact_message=" + contact_message;
                $.get(url, function (data) {
                    var status = data.Status;
                    var Message = data.Message;
                    if (status == "Success") {
                        $(".loader").hide();
                        successContant("Thank You ! We will contact you as soon as possible.", "green");
                    }
                    else {
                        $(".loader").hide();
                        messageModal(Message, "#ab1522");
                    }
                });
            }
            else {
                $(".loader").hide();
                messageModal("Please fill valid Phone number", "#ab1522");
            }
        }
        else {
            $(".loader").hide();
            messageModal("Please fill valid email address", "#ab1522");
        }
    }
});


/*--------------------------------------------------------------------------*/

/* contact us  part start here*/

/*--------------------------------------------------------------------------*/




 



