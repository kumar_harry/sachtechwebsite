<?php include("header.php"); ?>
<!----------------------------------------------------------------------------------------->
<!-- services part start here -->
<!----------------------------------------------------------------------------------------->
<div class="loader">
    <img src="images/159.gif" class="loader-img"/>
</div>

<div class="col-md-12 career_main_box nogutter">
    <div class="col-md-1"></div>
    <div class="col-md-10 career_main_container">
        <!--job opening end here-->
        <div class="row">
            <div class="career_top_heading_box">
                <h4 class="career_top_heading">Current Job Opening</h4>
                <hr>
            </div>
            <div class="col-md-3">
                <div class="row career-left-box-r">
                    <ul class="career-resp-tabs-list">
                        <li class="career-resp-tab-item career-resp-tab-active" id="career_ios"
                            aria-controls="tab_item-0" role="tab">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">iPhone Developer</a>
                            </div>
                        </li>
                        <li class="career-resp-tab-item" aria-controls="tab_item-2" role="tab" id="career_php">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">Php Developer</a>
                            </div>
                        </li>
                        <li class="career-resp-tab-item" aria-controls="tab_item-7" role="tab" id="career_android">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">Android Developer</a>
                            </div>
                        </li>
                        <li class="career-resp-tab-item" aria-controls="tab_item-1" role="tab" id="career_dotnet">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">.Net Developer</a>
                            </div>
                        </li>

                        <li class="career-resp-tab-item" aria-controls="tab_item-3" role="tab" id="career_seo">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">SEO</a>
                            </div>
                        </li>
                        <li class="career-resp-tab-item" aria-controls="tab_item-4" role="tab" id="career_graphic_designer">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">Graphic Designer</a>
                            </div>
                        </li>
                        <li class="career-resp-tab-item" aria-controls="tab_item-6" role="tab" id="career_digital">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">Digital Marketing</a>
                            </div>
                        </li>

                        <li class="career-resp-tab-item" aria-controls="tab_item-8" role="tab"
                            id="career_bussineeAnalyst">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">Business Analyst</a>
                            </div>
                        </li>
                        <li class="career-resp-tab-item" aria-controls="tab_item-9" role="tab" id="career_java">
                            <div class="career_product_tab_left">
                                <a href="javascript:;">Java Developer</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row career-right-box-r">
                    <div class="career_dev-type">
                        <h4 class="career_dev_heading" id="career_job_type"></h4>
                    </div>
                    <div class="career_skill_reqd career_ability_box">
                        <h4 class="career_heading">Skills Required</h4>
                        <p class="career_desc career_skill"></p>
                    </div>
                    <div class="career_no_opening career_ability_box">
                        <h4 class="career_heading">No. of Openings</h4>
                        <p class="career_desc career_number_opening"></p>
                    </div>
                    <div class="career_exp career_ability_box ">
                        <h4 class="career_heading">Experience</h4>
                        <p class="career_desc career_experince"></p>
                    </div>
                    <div class="career_quali career_ability_box">
                        <h4 class="career_heading">Qualification</h4>
                        <p class="career_desc career_qualification"></p>
                    </div>
                    <input name="apply" id="" value="Apply Now" type="submit" class="career_apply_now">
                </div>
            </div>
        </div>
        <!--job opening end here-->

        <!--news news part start here-->
        <div class="row career_news_room_row">
            <a id="career-news-room"></a>
            <div class="career_top_heading_box">
                <h4 class="career_top_heading">News Room</h4>
                <hr>
            </div>
            <div class="container-fluid">
                <div class="col-md-8">
                    <div class="row career_inr_box">
                        <div class="career_news_heading_box">
                            <h4 class="career_inr-news_heading">
                                <i class="fa fa-cubes" aria-hidden="true"></i>
                                Latest Technology
                            </h4>
                        </div>
                        <div class="career_news_text_box">
                            <img class="career_news_img img-responsive"
                                 src="images/Smart_City _Solutions_and_Applications_2017_6_27-94238.jpg"/>
                            <div class="news_text_box">
                                <p class="career_news_text">
                                    The Internet of Things is making its way into our everyday life, whether we notice
                                    it or not. Smart City deployments and solutions make up one of the largest IoT
                                    markets in the world. These applications are developed to increase citizen safety,
                                    save government money, reduce environment footprint and more. Here is a list of some
                                    of the top Smart City solutions.
                                </p>
                                <p class="career_news_text">
                                <h4 class="inner-news-text-head">Smart Parking</h4>
                                Smart Parking is perhaps the most adopted IoT smart city solution of all. Major cities
                                around the world are incorporating sensors and devices that count the number of cars in
                                a lot, notify for open spaces and even notify users when their parking time has expired.
                                There are many smart parking benefits that will help transform a city into a smart city.
                                </p>
                                <p class="career_news_text">
                                <h4 class="inner-news-text-head">Waste Management</h4>
                                The amount of trash is globally increasing at a very high rate. Cities will be forced to
                                become smarter by integrating automation solutions that manage this increase in waste.
                                This is being done by efforts such as smart trash sorting, truck optimization and even
                                underground pipes collecting household garbage.
                                </p>
                                <p class="career_news_text">
                                <h4 class="inner-news-text-head">Smart Street Lighting</h4>
                                Every city of lights has an extensive process to managing light posts, traffic lights
                                and any city-owned device with a bulb. By capturing and automating smart city data
                                correctly, this maintenance need could be reduced to a streamlined process from
                                monitoring and notifying to instillation and restocking.
                                </p>
                                <p class="career_news_text">
                                <h4 class="inner-news-text-head">Water Management</h4>
                                Cities must manage their drinking water to ensure that citizens have enough to safely
                                consume. Smart city water management can include optimized contamination monitoring,
                                leak detection, quality control, maintenance and more.
                                </p>
                                <p class="career_news_text">
                                <h4 class="inner-news-text-head">Smart Surveillance</h4>
                                Safety is key in major cities that experience crime in numbers. IoT physical security
                                can include intelligent monitoring, tampering alerts, perimeter protection, facial
                                recognition and automobile tracking.
                                </p>
                                <p class="career_news_text">
                                <h4 class="inner-news-text-head">Smart Buildings</h4>
                                The Internet of Things is also changing building management throughout cities by
                                introducing smart building devices and solutions. Office building managers can save
                                money and provide better service to tenants by incorporating systems like remote
                                monitoring, smart landscaping, IoT physical security and appliance maintenance.
                                </p>
                            </div>

                        </div>
                    </div>
                </div>

                <!--random news start here-->
                <div class="col-md-4">

                    <!--technonology full row links start here-->
                    <div class="row career_news_box">
                        <h1 class="career_quick_links_heading">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            <span class="career_quick_heading">Latest News</span>
                        </h1>
                        <div class="career_quick_links_box">
                            <ul class="career_quick_links_ul">
                                <a href="http://www.gadgetsnow.com/tech-news/amazon-launches-digital-wallet-to-take-on-paytm-flipkart/articleshow/60030943.cms"
                                   class="career_latest_ancor" >
                                    <li>
                                        <span>
                                            Amazon launches digital wallet to take on Paytm, Flipkart
                                        </span>
                                    </li>
                                </a>
                                <a href="http://www.gadgetsnow.com/slideshows/five-apps-that-connect-you-with-your-car/photolist/60030677.cms"
                                   class="career_latest_ancor" >
                                    <li>
                                        <span>
                                            Five apps that connect you with your car
                                        </span>
                                    </li>
                                </a>
                                <a href="http://www.gadgetsnow.com/tech-news/hbo-offers-250000-to-hackers-read-why/articleshow/60023803.cms"
                                   class="career_latest_ancor" >
                                    <li>
                                        <span>
                                            HBO offers $250,000 to 'hackers', read why
                                        </span>
                                    </li>
                                </a>
                                <a href="http://www.gadgetsnow.com/tech-news/its-no-more-intel-inside-samsung-beats-chip-giant/articleshow/59794544.cms"
                                   class="career_latest_ancor" >
                                    <li>
                                        <span>
                                            It's no more Intel inside, Samsung beats chip giant
                                        </span>
                                    </li>
                                </a>
                                <a href="https://www.newsbytesapp.com/timeline/Science/9749/54109/china-sends-unbreakable-quantum-code-to-earth"
                                   class="career_latest_ancor">
                                    <li>
                                        <span>
                                            China now sends unhackable messages from space to Earth
                                        </span>
                                    </li>
                                </a>
                            </ul>
                        </div>
                    </div>
                    <!--technonology full row end here-->
                </div>
                <!--random news end here-->
            </div>
        </div>
        <hr>
        <!--news news part end here-->

        <!--worshop at colleges start here-->
        <div class="row career_workshop_row">
            <a class="career-workshop"></a>
            <div class="career_top_heading_box">
                <h4 class="career_top_heading">Workshop at Colleges</h4>
                <hr>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active" align="center">
                                    <a class="serve-deve-bann-a" href="#">
                                        <img src="images/workshop/wrk2istr.jpg"
                                             class="career_workshop_img"/>
                                    </a>
                                </div>
                                <div class="item" align="center">
                                    <a class="serve-deve-bann-a" href="#">
                                        <img src="images/workshop/workshopimage.jpg"
                                             class="career_workshop_img"/> </a>
                                </div>
                                <div class="item" align="center">
                                    <a class="serve-deve-bann-a" href="#">
                                        <img src="images/workshop/wrk3.png"
                                             class="career_workshop_img"/>
                                    </a>
                                </div>
                            </div>
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example" data-slide-to="1"></li>
                                <li data-target="#carousel-example" data-slide-to="2"></li>
                            </ol>
                            <a class="left carousel-control" href="#carousel-example" role="button" data-slide="prev">
                                <span class="fa fa-chevron-left " style="margin-top:70px" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example" role="button" data-slide="next">
                                <span class="fa fa-chevron-right " style="margin-top:70px" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row career_workshop_content_box">
                            <p class="career_workshop_content_text">
                                SachTech Solution is specialized in delivering workshops on different technologies.
                                These workshop can be considered as a co-curricular activity that enriches the knowledge
                                base of student in an addition to what they learn at the college. The workshops are
                                delivered by the IT Professionals from the industry who share the real time experiences
                                with the student, hence enlightening them as per industry norms. These workshops not
                                only help the students to know about a particular technology but also help them to apt
                                for an appropriate career.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 career_workshop_content_box1 nogutter">
                    <p  class="career_workshop_content_text">
                        <span class="career_workshop_benefit">SKILL DEVELOPMENT WORKSHOPS BY SACHTECH SOLUTION</span>
                        With the market getting more competitive, the industry is looking for professionals who
                        are not only qualified but also skilled. The candidates equipped with skills apart from
                        a professional degree are more readily absorbed by the industry. Though the student
                        today are aware of this fact, but SachTech Solution is trying to emphasize on:
                    </p>

                    <p class="career_workshop_content_text">
                    <ul class="career_workshop_ul">
                        <li> Current developments in various technologies</li>
                        <li> Career aspects related to these technologies.</li>
                        <li> Develop and organize effective student learning outcomes.</li>
                    </ul>
                    </p>
                    <hr>
                    <p class="career_workshop_content_text">
                        <span class="career_workshop_benefit"> Benefit </span>
                    </p>
                    <p class="career_workshop_content_text">
                        <ul class="career_workshop_ul">
                            <li>
                                Sharpen your skills: Learn new ideas and approaches to make you more effective and
                                efficient at work.
                            </li>
                            <li>
                                Meet experts and influencers face-to-face: Conferences offer the opportunity to meet
                                business leaders and to position yourself as an expert in your field.
                            </li>
                            <li>
                                Absorb the energy of like-minded individuals: There’s nothing like being in a room of
                                like-minded people – other people who are willing to take time away from the office to
                                learn something new. Other people who want to “better” themselves.
                            </li>
                            <li>
                                Learn in a new space: Sitting in the same chair, in the same office or in the same
                                environment, can keep you from fresh thinking and new ideas. Breaking out of the office,
                                sitting in a new space, can help you uncover new approaches that will grow your
                                business.
                            </li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
        <!--worshop at colleges end   here-->
    </div>
    <div class="col-md-1"></div>
</div>
<!--modal start here-->
<div id="career_apply_modal" class="modal" style="">
    <!-- Modal content -->
    <div class="modal-dialog modal-mg">
        <div class="modal-content" style="width: 100%;">
            <span class="close" data-dismiss="modal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </span>
            <h4 class="modal-title career_submit_text">Submit Your Resume</h4>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!--modal end here-->
<?php include("footer.php"); ?>
<script>

    var urlData = window.location.href;
   if((urlData.indexOf("job=")) > 0)
    {
        var jobType = urlData.split("job=");
        var fieldId = jobType[1];
            $(".career-resp-tab-item").removeClass("career-resp-tab-active");
            $("#" + fieldId).addClass("career-resp-tab-active");
            switch (fieldId) {
                case "career_ios":
                    $(".career_dev_heading").html("iPhone Developer");
                    $(".career_skill").html("Xcode 7.x, AppCode");
                    $(".career_number_opening").html("2");
                    $(".career_experince").html("1 to 3 Years");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                case "career_php":
                    $(".career_dev_heading").html("Php Developer");
                    $(".career_skill").html("MVC, SQL Server,Angularjs, KnockoutjsJquery, Javascript,CMS");
                    $(".career_number_opening").html("1");
                    $(".career_experince").html("1 to 3 Years");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                case "career_android":
                    $(".career_dev_heading").html("Android Developer");
                    $(".career_skill").html("NDK, Worked with multiple 3rd party SDKs, Android studio, MVP, MVC & MVVM");
                    $(".career_number_opening").html("0");
                    $(".career_experince").html("1.5 to 2 Years");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                case "career_dotnet":
                    $(".career_dev_heading").html(".Net Developer");
                    $(".career_skill").html("ASP.NET, C#, MVC, SQL Server, WCF, Web APIEntity ramework, Angularjs, KnockoutjsJquery, Javascript");
                    $(".career_number_opening").html("0");
                    $(".career_experince").html("3 to 5 Years");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                case "career_seo":
                    $(".career_dev_heading").html("SEO");
                    $(".career_skill").html("Content writing, Grammatical good, having knowledge about SMM,Conversion Rate Optimization - CRO");
                    $(".career_number_opening").html("1");
                    $(".career_experince").html("Fresher");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                case "career_graphic_designer":
                    $(".career_dev_heading").html("Graphic Designer");
                    $(".career_skill").html("Visual Ideation / Creativity, Typography, Layout/Conversion Optimization" +
                        ", Print Design, Software(Photoshop, Illustrator, InDesign, Corel Draw)");
                    $(".career_number_opening").html("1");
                    $(".career_experince").html("3 to 5 Years");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                case "career_digital":
                    $(".career_dev_heading").html("Digital Marketing");
                    $(".career_skill").html("Good knowledge of Google & Bing webmaster tool, Experience with Googles Adwords Keyword Planner, SEOMoz, ahrefs, Semrush and other SEO tools.");
                    $(".career_number_opening").html("0");
                    $(".career_experince").html("4 to 5 Years");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                case "career_bussineeAnalyst":
                    $(".career_dev_heading").html("Business Analyst");
                    $(".career_skill").html("Problem Solving, Excellent Communication Skills, Data analyze, Knowledge about the wire-framing tools");
                    $(".career_number_opening").html("2");
                    $(".career_experince").html("1 Years");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                case "career_java":
                    $(".career_dev_heading").html("Java Developer");
                    $(".career_skill").html("<ul class='career_desc_ul'><li>Having knowledge of Spring, Hibernate and JPA, Microservices, Spring Boot.</li>" +
                        "<li>Should have worked on Database procedures and all major DBs including MSSQL, MYSQL, Postgres and Oracle.</li>" +
                        "<li>Knowledge of NoSQL DB will be good.</li>" +
                        "<li>Should have experience working on Webservices in JSON/SOAP.</li>" +
                        "<li>Knowledge of design patterns.</li></ul>");
                    $(".career_number_opening").html("0");
                    $(".career_experince").html("2 to 3 Years");
                    $(".career_qualification").html("B.Tech/MCA");
                    break;
                default:
                    break;
            }
    }
    else
    {
       careerContent();
        if((urlData.indexOf("scrl=")) > 0)
        {
            urlData = urlData.split("scrl=");
            $('html, body').animate({
                scrollTop: $("#"+urlData[1]).offset().top
            }, 800);
        }
    }


</script>
