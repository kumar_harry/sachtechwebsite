<?php include("header.php"); ?>
<!----------------------------------------------------------------------------------------->
<!-- services part start here -->
<!----------------------------------------------------------------------------------------->
<div class="loader">
    <img src="images/159.gif" class="loader-img"/>
</div>
<div class="col-md-12 services_main_box nogutter">
    <div class="col-md-1"></div>
    <div class="col-xs-12 col-md-10 services_main_container">
        <div class="container-fluid">
            <div class="row mobile-app-develpment">
                <div class="col-xs-12 col-md-6">
                    <div class="row serv-header-compdata android-services-data">
                        <h4 class="mobile-develop-text">
                            Mobile App Development Company
                        </h4>
                        <p class="mobile-dev-desc">
                            SachTech Solution is a leading mobile app development company amongst app development
                            companies. We offers advanced services in the field of mobile application development
                            across different platforms. SachTech Solution provides result-oriented mobile
                            application development services for Android, iPhone and iPad platforms.
                        </p>
                    </div>
                    <div class="row serv-header-compdata web-services-data">
                        <h4 class="mobile-develop-text">
                            Web Development
                        </h4>
                        <p class="mobile-dev-desc">
                            SachTech Solution is a web application development company, we have been creating quality
                            web applications since 2012. We can help embrace future of technology to gain competitive
                            advantage as you expand your business around the globe.
                            <br><br>
                            Over 100+ Web Apps Delivered On Time, Within Budget We deliver highest level of customer
                            service by deploying result-driven project management and seamless communication.
                        </p>
                    </div>
                    <div class="row serv-header-compdata opengl-services-data">
                        <h4 class="mobile-develop-text">
                            OpenGL
                        </h4>
                        <p class="mobile-dev-desc">
                            OpenGL is most widely adopted 2D and 3D graphics API in the industry and using OpenGL to
                            develop modern graphics applications. SachTech Solution provides the largest 3D body
                            manufacturer serving the fashion, medical and Fitness industries. We offer 3D configuration
                            integration in the field of the fashion and fitness. Our software enables 3D body analysis,
                            scanning, measurement and visualization. We have developers who are skillful in graphic
                            programming in different environments, be it mobile, web, desktop etc. They have experience
                            in OpenGL as well as integrating with complex 3D models. We are eager to work on new
                            challenges.
                        </p>
                    </div>
                    <div class="row serv-header-compdata digital-services-data">
                        <h4 class="mobile-develop-text">
                            Digital Marketing
                        </h4>
                        <p class="mobile-dev-desc">
                            Digital Marketing will help your business to gain online visibility and increase ROI. Our
                            services are designed to increase visibility within the algorithmic search results to
                            deliver high quality, targeted traffic to your website. SachTech Solution use “White Hat”
                            SEO practices to organically grow your rankings, instead of “Black Hat” - deceptive and
                            misleading - SEO. If you have not been regularly updating your SEO Service practices, you
                            are missing out. We offer the best SEO services, with experience in all aspects of buffing
                            up your web presence. We help you define your SEO objectives & develop a realistic strategy
                            with you.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 serv-prop-form-box">
                    <div class="row">
                        <h4 class="serv-prop-heading"> Request for Proposal </h4>
                        <p class="serv_confi_text_header">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            Safe & Confidential
                        </p>
                    </div>
                    <div class="row serv-purposal_form">
                        <div class="col-md-12">
                            <div class="row serv-purp-field-row">
                                <div class="col-md-4">
                                    <div class="serv-purp-field-box">
                                        <div class="serv-purp_icon">
                                            <img src="images/user.png" class="img-responsive"/>
                                        </div>
                                        <div class="serv-purp_field">
                                            <input type="text" placeholder="Name" id="client_name"
                                                   class="serv-purp-fields only-letters">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="serv-purp-field-box ">
                                        <div class="serv-purp_icon">
                                            <img src="images/at.png" class="img-responsive"/>
                                        </div>
                                        <div class="serv-purp_field">
                                            <input type="email" placeholder="Email Address" id="client_email"
                                                   class="serv-purp-fields">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="serv-purp-field-box serv-purp-no-border">
                                        <div class="serv-purp_icon">
                                            <img src="images/customer-service.png" class="img-responsive"/>
                                        </div>
                                        <div class="purp_field">
                                            <input type="text" placeholder="Mobile" id="client_contact"
                                                   class="serv-purp-fields only-numbers">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row serv-purp-field-row">
                                <div class="col-md-4">
                                    <div class="serv-purp-field-box">
                                        <div class="serv-purp_icon">
                                            <img src="images/coin.png" class="img-responsive"/>
                                        </div>
                                        <div class="purp_field">
                                            <select class="serv-purp-select-box" id="client_budget"
                                                    aria-hidden="false">
                                                <option value="">Your Budget</option>
                                                <option value="Less Than $1000 ">Less Than $1000</option>
                                                <option value="$1000 - $3000">$1000 - $3000</option>
                                                <option value="$3000 - $7000">$3000 - $7000</option>
                                                <option value="More Than $7000">More Than $7000</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="serv-purp-field-box ">
                                        <div class="serv-purp_icon">
                                            <img src="images/placeholder.png" class="img-responsive"/>
                                        </div>
                                        <div class="purp_field">
                                            <select id="countryId" class="countries serv-purp-select-box"
                                                    id="client_country" aria-hidden="false">

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="serv-purp-field-box serv-purp-no-border">
                                        <div class="serv-purp_icon">
                                            <img src="images/chat.png" class="img-responsive"/>
                                        </div>
                                        <div class="serv-purp_field">
                                            <input type="text" placeholder="Skype/Whatsapp" id="client_social_id"
                                                   class="serv-purp-fields">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row serv-purp-field-row">
                                <div class="col-md-12">
                                    <div class="serv-purp-field-box serv-purp-no-border">
                                        <div class="serv-purp_icon">
                                            <img src="images/infr-circular-button.png" class="img-responsive"/>
                                        </div>
                                        <div class="serv-purp_desc_field">
                                                <textarea placeholder="Project Description" rows="3" cols="40"
                                                          class="serv-purp-textarea-fields"
                                                          id="client_description"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-xs-12 col-md-12 nogutter">
                                    <div class="serv-purp-field-box serv-purp-no-border ">
                                        <div class="serv-purp_submit_field">
                                            <input type="button" class="serv-purp-submit-btn"
                                                   value="Submit a Request" id="purp-submit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clear"></div>
            <div class="row service-content-row">
                <a class="serv-refrd-sachtech"></a>
                <div class="why-sachtech-box-heading serv-header-compdata android-services-data">
                    <h4 class="serv-why-sach-head">
                        Why SachTech Solution is first choice for clients when it comes to selecting from top mobile
                        app development companies?
                    </h4>
                    <div class="serv-y-sach-desc">
                        <ul class="serv-y-sach-ul">
                            <li class="saerv-y-sach-li">
                                We have a strong team of 40+ android app developers, iOS app developer and windows
                                app developer who are well-versed to build apps.
                            </li>
                            <li class="saerv-y-sach-li">
                                With offices in Canada and India, we are an app creator firms having global clients.
                            </li>
                            <li class="saerv-y-sach-li">
                                SachTech Solution successfully deploying more than 100+ Smartphones apps in
                                google play store and apple store.
                            </li>
                            <li class="saerv-y-sach-li">
                                We apply embedded software development techniques for better solutions that are
                                compatible with different mobile phones.
                            </li>
                            <li class="saerv-y-sach-li">
                                We also provide the complete solution like integration for all platform like
                                embedded system connected with android and android connected with website.
                            </li>
                            <li class="saerv-y-sach-li">
                                We have flexible working hours and can be available in your time zone via YAHOO,
                                MSN, GTALK, Skype or whats app.
                            </li>
                            <li class="saerv-y-sach-li">
                                We have huge experience in creating versatile, innovative engaging and
                                user-focused apps for phone across myriad of categories at competitive prices.
                            </li>
                            <li class="saerv-y-sach-li">
                                We are currently building apps for Android Nougat and iOS 10.
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="why-sachtech-box-heading serv-header-compdata web-services-data">
                    <h4 class="serv-why-sach-head">
                        Why SachTech Solution is first choice for clients when it comes to selecting from top Web
                        app development companies?
                    </h4>
                    <div class="serv-y-sach-desc">
                        <ul class="serv-y-sach-ul">
                            <li class="saerv-y-sach-li">
                                We have a strong team of 30+ web app developers, who are well-versed to build web
                                applications.
                            </li>
                            <li class="saerv-y-sach-li">
                                With offices in Canada and India, we are an app creator firms having global clients.
                            </li>
                            <li class="saerv-y-sach-li">
                                SachTech Solution successfully deploying more than 200+ web applications developed in
                                Java, Php, .Net,
                                Node Js, angularjs language.
                            </li>
                            <li class="saerv-y-sach-li">
                                We apply embedded software development techniques for better solutions that are
                                compatible with different web aplications.
                            </li>
                            <li class="saerv-y-sach-li">
                                We also provide the complete solution like integration for all platform like
                                embedded system connected with android and android connected with website.
                            </li>
                            <li class="saerv-y-sach-li">
                                We have flexible working hours and can be available in your time zone via YAHOO,
                                MSN, GTALK and Skype.
                            </li>
                            <li class="saerv-y-sach-li">
                                We have huge experience in creating versatile, innovative engaging and
                                user-focused apps for phone across myriad of categories at competitive prices.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <!--android start here-->
                    <div class="container-fluid serv-header-compdata android-services-data">
                        <a id="mobileSection"></a>
                        <div class="row">
                            <h2 class="serv-capability-text">
                                Our Capabilities
                            </h2>
                            <p class="serv-capability-desc">
                                SachTech Solution offers exceptional solutions for mobile application development by
                                extracting
                                the maximum potential out of major mobile technologies. Our major capabilities in
                                mobile application development are mentioned below along with the tools,
                                technologies and programming languages used by us for deploying reliable solutions
                                for each platform.
                            </p>
                            <div class="serv-capability-box">
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Android</th>
                                    </tr>
                                    <tr>
                                        <td class="table-text" align="left" colspan="2">Android is an open source mobile
                                            application
                                            platform offering high levels of flexibility and opportunities for both
                                            developers and users.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="table-text" valign="top"><span class="bold">Tools</span></td>
                                        <td class="table-text" valign="top" width="75%">Eclipse</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">Android SDK, Android Native Development Kit
                                            (NDK), Android DT (ADT)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">Java, C/C++, XML</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="serv-capability-box">
                                <a class="serv-ios-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">iOS / iPhone / iPad</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">iPhones are a very hot commodity in the mobile
                                            market today. A very large portion of smartphone users is dedicated to
                                            iPhone and iPad devices.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Xcode</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">iPhone SDK, Cocoa Touch</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">Objective-C</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-windowapp-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Windows phone app Development</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">We are able to create the full range of
                                            applications that you might want for the Windows Phone 8 operating system.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Visual Studio</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">Windows Platform (UWP) apps for tablet, phone, PC,
                                            Windows IoT, or Xbox
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">C++, C#, Microsoft Visual Basic, and JavaScript
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-appmaintainance-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">App Maintenance</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">SachTech Solution engineers can help you improve
                                            your mobile app of any scale through our app maintenance and enhancement
                                            services.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Eclipse, Xcode, Visual Studio</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">iPhone SDK, Cocoa Touch</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">Objective-C</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--android end here-->
                    <!--web start here-->
                    <div class="container-fluid serv-header-compdata web-services-data">
                        <a id="webSection"></a>
                        <div class="row">
                            <!-- <h2 class="serv-capability-text">
                                 Our Capabilities
                             </h2>-->
                            <div class="serv-capability-box">
                                <a class="serv-java-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Java Development</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">Java development services from SachTech Solution
                                            include full cycle of software development from consulting to product
                                            support. Our Java programmers can make high-end applications that conform to
                                            your plans and business goals.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">IntelliJ IDEA, Eclipse IDE, NetBeans</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">Java development kit</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">Java</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-cms-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Word Press</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">WordPress is a Content Management System we use to
                                            build great websites. It is web-based, so managing your website's content is
                                            simple and straightforward.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Sublime, Notepad++</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">Xampp, Wamp</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">CMS</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-ecommerce-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">E-commerce Development</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">eCommerce web development and designing services
                                            have become a necessity in order to drive a huge mass of traffic on your
                                            online store
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Sublime, Notepad++</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">Magento, Shopify, Woo Commerce, OpenCart, Big
                                            Commerce
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">CMS</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-webdesigning-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Web Designing Services</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">Web designing basically refers to the front end
                                            design of a website which makes or breaks an entity’s impression.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Sublime, Notepad++</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">Chrome, Firefox, Microsoft Edge</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">Html, Html5, CSS3, Bootstrap, Foudation, Flex</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-webmaintainance-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Web Site Maintainance</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2"> SachTech Solution website maintenance services
                                            team works with you to keep your website as professional as you are.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Sublime, Notepad++, IntelliJ IDEA</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">Xampp, Wamp</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%"> Html, Html5, CSS3, Bootstrap, Foudation, Flex,
                                            CMS
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--web end here-->
                    <!--open gl start here-->
                    <div class="container-fluid serv-header-compdata opengl-services-data">
                        <a id="opengl"></a>
                        <div class="row">
                            <div class="serv-capability-box">
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Open Gl</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            OpenGL is most widely adopted 2D and 3D graphics API in the industry and
                                            using OpenGL to develop modern graphics applications
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">BuGLe, AMD CodeXL, GLSLang, KTX</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">NDK</td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">Android, iOS</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-logo-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Logo Design</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">We offer Logo design services to suit every
                                            business’ needs. All logos are custom designed by our skilled and
                                            experienced logo designers.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Photoshop, Coreldraw</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-flash3d-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Flash and 3d Animation</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">SachTech Solution provides you the best range of 2d
                                            flash animation services, 3d flash animation services, 3d film animation
                                            services, 3d interior designing services & 3d modelling services with
                                            effective & timely delivery.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Flash, Toon Boom, CreaToon</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-psdtoxhtml-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Psd to Xhtml</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">SachTech Solution offers pixel perfect PSD to HTML,
                                            PSD to XHTML, PSD to CSS conversion services with W3C compliance.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">PSD to WEB, PSD to CSS, Corel Draw</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--opengl end start here-->
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="clear"></div>
            <!--seo part start here-->
            <div class="row serve-seo-section serv-header-compdata digital-services-data">
                <div class="container-fluid serve-seo-main-box">
                    <div class="row serve-seo-head-box">
                        <h4 class="serve_seo_heading">
                            Our Popular Packages
                        </h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 serv-seo-heading">
                            <ul>
                                <li class="tab"><a id="global-seo" class="seo-heading-list seo-head-active">Local
                                        SEO</a></li>
                                <li class="tab"><a id="ecommerce-seo" class="seo-heading-list">E-commerce SEO</a></li>
                                <li class="tab"><a id="local-seo" class="seo-heading-list">Global SEO</a></li>
                                <div class="clear"></div>
                            </ul>
                        </div>
                    </div>
                    <div class="row serve-seo-content-box">
                        <div class="col-md-8">
                            <div class="row serve-seo-cont-box">
                                <div class="serve-seo-package-list global-seo-content">
                                    <div class="serve-seo-head">Common Features of Local SEO Package:</div>
                                    <ul>
                                        <li>H card integration</li>
                                        <li class="last">Google Places listing request</li>
                                        <li>Page updation for local search</li>
                                        <li class="last">User reviews</li>
                                        <li>Citations in other sites/directories</li>
                                    </ul>
                                </div>
                                <div class="serve-seo-package-list ecommerce-seo-content">
                                    <div class="serve-seo-head">Common Features of E-commerce package:</div>
                                    <ul>
                                        <li>Product description analysis</li>
                                        <li class="last">Site Architecture recommendations</li>
                                        <li>Landing page analysis and recommendations</li>
                                        <li class="last">Conversion analysis and recommendations</li>
                                        <li>Heatmap reports to show actual user behavior</li>
                                    </ul>
                                </div>

                                <div class="serve-seo-package-list local-seo-content">
                                    <div class="serve-seo-head">Common Features of Global SEO:</div>
                                    <ul>
                                        <li>Site architecture analysis and recommendations</li>
                                        <li class="last">Geo-targetting</li>
                                        <li>Google place listing</li>
                                        <li class="last">Sub-domain and sub-folder optimization</li>
                                        <li>Location specific global link strategy</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row serve-seo-left-image global-seo-content">
                                <img src="http://cdn.pagetraffic.in/images/global-seo-img.png" class="img-responsive"/>
                            </div>

                            <div class="row serve-seo-left-image local-seo-content">
                                <img src="http://cdn.pagetraffic.in/images/local-seo-img.png" class="img-responsive"/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--seo part end here-->
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="container-fluid serv-header-compdata digital-services-data">

                        <div class="row">
                            <div class="serv-capability-box">
                                <a class="serv-emailmarketing-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Email Marketing</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">Email marketing is a form of direct marketing which
                                            uses electronic mail as a means of communicating commercial.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Litmus, Mail Chimp, Mad Mimi, Target Hero</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="serv-capability-box">
                                <a class="serv-swmaintainance-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Software Maintenance</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">SachTech Solution engineers can help you improve
                                            your software of any scale through our software maintenance and enhancement
                                            services.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Tools</span></td>
                                        <td valign="top" width="75%">Eclipse, NetBeans, IntelliJ IDEA, Eclipse IDE,
                                            NetBeans
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Technologies</span></td>
                                        <td valign="top" width="75%">Development Kit
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="bold">Languages</span></td>
                                        <td valign="top" width="75%">Java</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="serv-capability-box">
                                <a class="serv-outsourcing-section"></a>
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th align="left" colspan="2">Outsourcing</th>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">Besides various readymade SachTech Solution
                                            business process outsourcing solutions for various processes like Mobile app
                                            services, Onsite Database Administration Services, Online Shopping industry
                                            etc. We have specialization in various industrial critical, technical and
                                            general processes.
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>

            <div class="clear"></div>
            <!-- hire developer part start here-->
            <div class="row serve-devel-section ">
                <div class="col-md-8">
                    <div class="container-fluid">
                        <div class="col-md-12 nogutter">
                            <h4 class="serve-benefit-heading">
                                SachTech Solution Value-Added Benefits
                            </h4>
                        </div>
                    </div>
                    <div class="row serve-benefit-list">
                        <div class="col-md-7">
                            <div class="row">
                                <ul class="serve-check-list">
                                    <li>Flexible Engagement Models</li>
                                    <li>40+ highly skilled IT professionals with an average 5+ years
                                        experience
                                    </li>
                                    <li>5+ years in IT business</li>
                                    <li>Seamless communication through Phone/Skype/Chat/Email</li>
                                    <li>100% Satisfaction Guarantee</li>
                                    <li>Competitive pricing and on-time delivery</li>
                                    <li>24/7 support accross all timezones</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <div class="serve-developer-img">
                                    <img src="images/hireuser.png"
                                         class="img-responsive">
                                </div>
                                <div class="serve-developer-hiretext">
                                    <p>Hire Our Developers</p>
                                    <span class="bold serve-devl-hire-amount">$15/hr</span>
                                </div>
                            </div>
                            <div class="row serve-hire-desc">
                                <ul>
                                    <li>24/7 Technical Support</li>
                                    <li>100% Confidentiality Assured <br>
                                        <span class="f11">(Strict NDA Terms)</span></li>
                                    <li>100% Moneyback Guarantee</li>
                                </ul>
                                <div class="serve-getstarted-box">
                                    <a href="request_purposal"
                                       class="buttons serve-red_small">Get Started Now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="container-fluid">
                        <div class="col-md-12 nogutter">
                            <h4 class="serve-benefit-heading">
                                Examples & Testimonial
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-12 serv-devel-banner-box12">
                        <div class="row serv-devel-banner android-services-slider-data">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="serve-deve-bann-a" href="#">
                                            <img src="images/training/android/royalmunch/11_nexus4_landscape.png"
                                                 class="serv-deve-ban-img"/>
                                            <div class="serve-banner-desc">
                                                <p>
                                                    Was a pleasure to work with. SachTech Solution team was able to
                                                    create the app according to my specs. They were more than willing to
                                                    make changes as we went. Final product looks great.
                                                </p>
                                                <!--<span>SBS Communications, Ltd.</span>-->
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="serve-deve-bann-a" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142846_0_nexus4_angle1.png"
                                                 class="serv-deve-ban-img"/>
                                            <div class="serve-banner-desc">
                                                <p>
                                                    Very Patient and takes time to complete whatever is required.
                                                    Responded great to new requirements and got it done .
                                                    Highly satisfied with the quality of work and the professionalism
                                                    and dedication showed towards the work.
                                                </p>
                                                <!--<span>SBS Communications, Ltd.</span>-->
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="serve-deve-bann-a" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-07-11_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="serv-deve-ban-img"/>
                                            <div class="serve-banner-desc">
                                                <p>
                                                    It was very nice working with him. I truly recommend everyone to
                                                    give him an opportunity to serve you with his best deliverables. He
                                                    is the BEST.
                                                </p>
                                                <!--<span>SBS Communications, Ltd.</span>-->
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="serve-deve-bann-a" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141812_0_nexus4_landscape.png"
                                                 class="serv-deve-ban-img"/>
                                            <div class="serve-banner-desc">
                                                <p>
                                                    SachTech is definitely a freelancer to choose if you are looking for
                                                    an efficient programmer to work with. Their response time, their
                                                    determination on finishing their task are to be applaud, definitely
                                                    one of the best if not the best programmer on Upwork.
                                                </p>
                                                <!--<span>SBS Communications, Ltd.</span>-->
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- <ol class="carousel-indicators">
                                     <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                                     <li data-target="#carousel-example" data-slide-to="1"></li>
                                     <li data-target="#carousel-example" data-slide-to="2"></li>
                                 </ol>-->
                            </div>
                        </div>
                        <div class="row serv-devel-banner web-services-slider-data">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="serve-deve-bann-a" href="#">
                                            <img src="images/training/web/disthi%20website/first.PNG"
                                                 class="serv-deve-ban-img"/>
                                            <div class="serve-banner-desc">
                                                <p>
                                                    Was a pleasure to work with. SachTech Solution team was able to
                                                    create the app according to my specs. They were more than willing to
                                                    make changes as we went. Final product looks great.
                                                </p>
                                                <!--<span>SBS Communications, Ltd.</span>-->
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="serve-deve-bann-a" href="#">
                                            <img src="images/training/web/Scan2Tailor/second.PNG"
                                                 class="serv-deve-ban-img"/>
                                            <div class="serve-banner-desc">
                                                <p>
                                                    Very Patient and takes time to complete whatever is required.
                                                    Responded great to new requirements and got it done .
                                                    Highly satisfied with the quality of work and the professionalism
                                                    and dedication showed towards the work.
                                                </p>
                                                <!--<span>SBS Communications, Ltd.</span>-->
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="serve-deve-bann-a" href="#">
                                            <img src="images/training/web/size%20usa/first.PNG"
                                                 class="serv-deve-ban-img"/>
                                            <div class="serve-banner-desc">
                                                <p>
                                                    It was very nice working with him. I truly recommend everyone to
                                                    give him an opportunity to serve you with his best deliverables. He
                                                    is the BEST.
                                                </p>
                                                <!--<span>SBS Communications, Ltd.</span>-->
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="serve-deve-bann-a" href="#">
                                            <img src="images/training/web/Spinner%20Force/third.PNG"
                                                 class="serv-deve-ban-img"/>
                                            <div class="serve-banner-desc">
                                                <p>
                                                    SachTech is definitely a freelancer to choose if you are looking for
                                                    an efficient programmer to work with. Their response time, their
                                                    determination on finishing their task are to be applaud, definitely
                                                    one of the best if not the best programmer on Upwork.
                                                </p>
                                                <!--<span>SBS Communications, Ltd.</span>-->
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- <ol class="carousel-indicators">
                                     <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                                     <li data-target="#carousel-example" data-slide-to="1"></li>
                                     <li data-target="#carousel-example" data-slide-to="2"></li>
                                 </ol>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- hire developer part end here-->
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="clear"></div>
<!----------------------------------------------------------------------------------------->
<!-- services part end here -->
<!----------------------------------------------------------------------------------------->
<?php include("footer.php"); ?>
<script>
    /*for get data according to service type in url*/
    serviceData();
</script>
