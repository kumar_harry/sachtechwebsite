<?php include("header.php"); ?>
<!----------------------------------------------------------------------------------------->
            <!-- request puposal content start here -->
<!----------------------------------------------------------------------------------------->
<div class="loader">
    <img src="images/159.gif" class="loader-img" />
</div>
<div class="col-md-12 req_pur_main_box nogutter">
    <div class="col-md-1"></div>
    <div class="col-md-10 req_pur_main_box1">
        <div class="container-fluid req_pur_inner_box">
            <div class="col-md-12 req_pur_inner_box12">
                <div class="row">
                    <h4 class="req_pur_heading">
                        Request for Proposal
                        <p class="safe_confi_text">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            Safe & Confidential
                        </p>
                    </h4>
                </div>
                <div class="row purposal_form">
                    <div class="col-md-12">
                        <div class="row purp-field-row">
                            <div class="col-md-4">
                                <div class="purp-field-box">
                                    <div class="purp_icon">
                                        <img src="images/user.png" class="img-responsive" />
                                    </div>
                                    <div class="purp_field">
                                        <input type="text" placeholder="Name" id="client_name" class="purp-fields only-letters">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="purp-field-box ">
                                    <div class="purp_icon">
                                        <img src="images/at.png" class="img-responsive" />
                                    </div>
                                    <div class="purp_field">
                                        <input type="email" placeholder="Email Address" id="client_email" class=" purp-fields">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="purp-field-box purp-no-border">
                                    <div class="purp_icon">
                                        <img src="images/customer-service.png" class="img-responsive" />
                                    </div>
                                    <div class="purp_field">
                                        <input type="text" placeholder="Contact Number" id="client_contact" class="purp-fields only-numbers">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row purp-field-row">
                            <div class="col-md-4">
                                <div class="purp-field-box">
                                    <div class="purp_icon">
                                        <img src="images/coin.png" class="img-responsive" />
                                    </div>
                                    <div class="purp_field">
                                        <select class="purp-select-box" id="client_budget"  aria-hidden="false">
                                            <option value="">Your Budget</option>
                                            <option value="Less Than $1000 ">Less Than $1000 </option>
                                            <option value="$1000 - $3000">$1000 - $3000</option>
                                            <option value="$3000 - $7000">$3000 - $7000</option>
                                            <option value="More Than $7000">More Than $7000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="purp-field-box ">
                                    <div class="purp_icon">
                                        <img src="images/placeholder.png" class="img-responsive" />
                                    </div>
                                    <div class="purp_field">
                                        <select id="countryId" class="countries purp-select-box" id="client_country" aria-hidden="false">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="purp-field-box purp-no-border">
                                    <div class="purp_icon">
                                        <img src="images/chat.png" class="img-responsive" />
                                    </div>
                                    <div class="purp_field">
                                        <input type="text" placeholder="Skype ID/ Whatsapp No. " id="client_social_id" class="purp-fields">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row purp-field-row">
                            <div class="col-md-12">
                                <div class="purp-field-box purp-no-border">
                                    <div class="purp_icon">
                                        <img src="images/infr-circular-button.png" class="img-responsive" />
                                    </div>
                                    <div class="purp_desc_field">
                                        <textarea placeholder="Project Description" rows="8" cols="40"  class="purp-textarea-fields"
                                                  id="client_description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-md-12 purp_boxbtn_top">
                                <div class="purp-field-box purp-no-border">
                                    <div class="purp_submit_field">
                                        <input type="button" class="purp-submit-btn" value="Submit a Request" id="purp-submit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">

            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="clear"></div>
<!----------------------------------------------------------------------------------------->
            <!-- request puposal content end here -->
<!----------------------------------------------------------------------------------------->
<?php include("footer.php"); ?>