<?php
    include_once 'header.php';
?>
<!--slider start here-->
<div class="col-xs-12 col-md-12 nogutter">
    <div id="carousel-example" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            Dreams are not those which comes while we are sleeping, but dreams are those when you don't sleep before fulfilling them.
                        </p>
                    </div>
                    <img src="images/slider/companypro2.jpg" />
                </a>
            </div>
            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            SachTech Solution want to have one mission and target: Take the nation forward - Digitally and Economically.
                        </p>
                    </div>
                    <img src="images/slider/shutterstock_436458775.jpg" />
                </a>
            </div>

            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            The Future of Mobile means a more intricately connected ecosystem of applications.
                        </p>
                    </div>
                    <img src="images/slider/iphone410311.jpg" />
                </a>
            </div>
            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            SachTech Solution offer complete responsive Web Design & Development Services.
                        </p>
                    </div>
                    <img src="images/slider/shutterstock_122664076.jpg"  />
                </a>
            </div>
            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            Get end-to-end Mobile app development services from a leading mobile application development company
                        </p>
                    </div>
                    <img src="images/slider/shutterstock_206441323.jpg"/>
                </a>
            </div>
            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            SachTech Solution controls electrical loads by making use of an android application.
                        </p>
                    </div>
                    <img src="images/slider/shutterstock_289867238.jpg" />
                </a>
            </div>
            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            Learn skills & strategies for creating a better world through your life & work!
                        </p>
                    </div>
                    <img src="images/slider/shutterstock_293185730.jpg" />
                </a>
            </div>

            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            Google only loves you when everyone else loves you first
                        </p>
                    </div>
                    <img src="images/slider/facebook424521.jpg" />
                </a>
            </div>
            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view">
                        <p class="slider_cart_desc">
                            Everything that can be automated will be automated
                        </p>
                    </div>
                    <img src="images/slider/shutterstock_595866521.jpg" />
                </a>
            </div>
            <div class="item" align="center">
                <a class="serve-deve-bann-a" href="#">
                    <div class="cart_view" style="left: 850px">
                        <p class="slider_cart_desc">
                            Customer satisfaction is a measure of our success.
                        </p>
                    </div>
                    <img src="images/slider/shutterstock_384506902.jpg" /></a>
                </a>
            </div>

        </div>
        <ol class="carousel-indicators index_carousal_indicator">
            <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example" data-slide-to="1"></li>
            <li data-target="#carousel-example" data-slide-to="2"></li>
            <li data-target="#carousel-example" data-slide-to="3"></li>
            <li data-target="#carousel-example" data-slide-to="4"></li>
            <li data-target="#carousel-example" data-slide-to="5"></li>
            <li data-target="#carousel-example" data-slide-to="6"></li>
            <li data-target="#carousel-example" data-slide-to="7"></li>
            <li data-target="#carousel-example" data-slide-to="8"></li>
            <li data-target="#carousel-example" data-slide-to="9"></li>
        </ol>
        <a class="left carousel-control" href="#carousel-example" role="button" data-slide="prev">
            <span class="fa fa-chevron-left index_slider_controller" style="" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example" role="button" data-slide="next">
            <span class="fa fa-chevron-right index_slider_controller" style="" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="svg-element-slider-bottom slider_bottom index_slider_botImg"></div>

</div>
<div class="clear"></div>
<!--slider end here-->

<!--services tab start here-->
<div class="col-md-12">
    <div class="row services_head_box">
        <h2 class="services-heading headings">SERVICES</h2>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 services-box-m1">
            <div class="row services-box-row">
                <a href="services?service=service-ios">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #7b184f">
                            <h3 class="service-name">
                                iOS App Development
                            </h3>
                            <div class="service-description">
                                <h3>iOS App Development</h3>
                                <p>Native and cross-platform mobile app development</p>
                            </div>
                            <img src="images/Apple Filled-50.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-android">
                    <div class="col-md-6 services-innerbox" >
                        <div class="service-cart-box" style="background: #c32883">
                            <h3 class="service-name">
                                Android App Development
                            </h3>
                            <div class="service-description">
                                <h3>Android App Development</h3>
                                <p>Native and cross-platform mobile app development</p>
                            </div>

                            <img src="images/Android OS-80.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-windowapp">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #db3356">
                            <h3 class="service-name">
                                Window App Development
                            </h3>
                            <div class="service-description">
                                <h3>Window App Development</h3>
                                <p>Native and cross-platform mobile app development</p>
                            </div>

                            <img src="images/windows.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="row services-box-row">
                <a href="services?service=service-opengl">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #a5232f">
                            <h3 class="service-name">
                                Open GL
                            </h3>
                            <div class="service-description">
                                <h3>Open GL</h3>
                                <p>OpenGL is the most widely adopted 2D and 3D graphics</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-appmaintainance">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #e17200">
                            <h3 class="service-name">
                                App Maintenance
                            </h3>
                            <div class="service-description">
                                <h3>App Maintenance</h3>
                                <p>Native and cross-platform mobile app Maintenance</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-seo">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #c32883">
                            <h3 class="service-name">
                                SEO
                            </h3>
                            <div class="service-description">
                                <h3>SEO</h3>
                                <p>Google Only loves you when everyone else loves you first</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-phpdevelopment">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #7b184f">
                            <h3 class="service-name">
                                Php Development
                            </h3>
                            <div class="service-description">
                                <h3>Php Development</h3>
                                <p>Offer a full range of web design development services</p>
                            </div>

                            <img src="images/PHP Logo-50.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="row services-box-row">
                <a href="services?service=service-javadevelopment">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #c32883">
                            <h3 class="service-name">
                                Java Development
                            </h3>
                            <div class="service-description">
                                <h3>Java Development</h3>
                                <p>Offer a full range of web design development services</p>
                            </div>

                            <img src="images/Java-50.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-ecommerce">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #db3356">
                            <h3 class="service-name">
                                E-Commerce Development
                            </h3>
                            <div class="service-description">
                                <h3>E-Commerce Development</h3>
                                <p>Offer a full range of web design development services</p>
                            </div>

                            <img src="images/E Learning-50.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-cms">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #a5232f">
                            <h3 class="service-name">
                                CMS Development
                            </h3>
                            <div class="service-description">
                                <h3>CMS Development</h3>
                                <p>Offer a full range of web design development services</p>
                            </div>

                            <img src="images/WordPress-50.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-softwaremaintainance">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #e17200">
                            <h3 class="service-name">
                                Software Maintenance
                            </h3>
                            <div class="service-description">
                                <h3>Software Maintenance</h3>
                                <p>Offer a full range of web design development maintenance</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="row services-box-row">
                <a href="services?service=service-emailmarketing">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #a5232f">
                            <h3 class="service-name">
                                Email Marketing
                            </h3>
                            <div class="service-description">
                                <h3>Email Marketing</h3>
                                <p>Integrated email marketing, marketing automation, and small business CRM</p>
                            </div>

                            <img src="images/Email-50.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-webdesigning">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #c32883">
                            <h3 class="service-name">
                                Web Designing Services
                            </h3>
                            <div class="service-description">
                                <h3>Web Designing Services</h3>
                                <p>Offer a full range of web design development services</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-psdtoxhtml">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #7b184f">
                            <h3 class="service-name">
                                PSD To XHTML
                            </h3>
                            <div class="service-description">
                                <h3>PSD To XHTML</h3>
                                <p>We convert your design PSD to HTML and XHTML / CSS</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-flash3d">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #db3356">
                            <h3 class="service-name">
                                Flash & 3d Animation
                            </h3>
                            <div class="service-description">
                                <h3>Flash & 3d Animation</h3>
                                <p>logo designers always have words that inspire you to be creative</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="row services-box-row">
                <a href="services?service=service-logo">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #c32883">
                            <h3 class="service-name">
                                Logo Design
                            </h3>
                            <div class="service-description">
                                <h3>Logo Design</h3>
                                <p>logo designers always have words that inspire you to be creative</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-websitemaintainance">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #7b184f">
                            <h3 class="service-name">
                                Website Maintenance
                            </h3>
                            <div class="service-description">
                                <h3>Website Maintenance</h3>
                                <p>Offer a full range of web design development maintenance</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="services?service=service-outsourcing">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #a5232f">
                            <h3 class="service-name">
                                IT Strategy and Consulting
                            </h3>
                            <div class="service-description">
                                <h3>IT Strategy and Consulting</h3>
                                <p>IT Strategy and Consulting Description</p>
                            </div>

                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
                <a href="skill?section-IOT">
                    <div class="col-md-3 services-innerbox" >
                        <div class="service-cart-box" style="background: #e17200">
                            <h3 class="service-name">
                                Internet of Things
                            </h3>
                            <div class="service-description">
                                <h3>Internet of Things</h3>
                                <p> Enables you to connect with people, applications and things.</p>
                            </div>
                            <img src="images/iconn-three-new.png" class="mini-svg">
                            <span href="#" class="target-to"></span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- curve image slider curve image -->
<!--<div class="col-md-12 slantingdiv"></div>-->
<div class="clear"></div>
<!--services tab end here  -->
<!--quick links start here-->
<div class="col-md-12">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row strip-box">
            <div class="ltst_updtscntr marquee_strip">
                <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" >
                    <span class="last">&nbsp;
                        <a   class="strip_ancor">
                            For any query contact us @ <i class="fa fa-phone-square" aria-hidden="true"></i> 7087425288,
                            <i class="fa fa-phone-square" aria-hidden="true"></i> 7087972568
                        <span class="newimg" style="border-right: 0 solid #333;"></span>
                      </a>
                    </span>
                </marquee>
            </div>
        </div>
        <div class="row">
            <!--quick links start here-->
            <div class="col-md-3">
                <div class="row news_box">
                    <h1 class="quick_links_heading">
                        <i class="fa fa-pencil"></i>
                        <span class="quick_heading">Quick Links</span>
                    </h1>
                    <div class="quick_links_box">
                        <ul class="quick_links_ul">
                            <a href="https://play.google.com/store/apps/details?id=sachtech.stsmentor.android.java.php.net"  class="quick_ancor">
                                <li class="quick_menu_items">
                                    <i class="fa fa-play quick_play" aria-hidden="true"></i>
                                    <span class="quick_text"> STS Mentor </span>
                                    <span class="quick_newimg"></span>
                                </li>
                            </a>

                            <a href="https://play.google.com/store/apps/details?id=com.sachtech.expense.accontancy"  class="quick_ancor">
                                <li class="quick_menu_items">
                                    <i class="fa fa-play quick_play" aria-hidden="true"></i>
                                    <span class="quick_text"> Personal Expense Manager </span>
                                    <span class="quick_newimg"></span>
                                </li>
                            </a>

                            <a href="https://play.google.com/store/apps/details?id=salon.look.ui" class="quick_ancor">
                                <li class="quick_menu_items">
                                    <i class="fa fa-play quick_play" aria-hidden="true"></i>
                                    <span class="quick_text"> GetLook </span>

                                </li>
                            </a>

                            <a href="https://play.google.com/store/apps/details?id=kapilsony.sachtech.com.todomanager"  class="quick_ancor">
                                <li class="quick_menu_items">
                                    <i class="fa fa-play quick_play" aria-hidden="true"></i>
                                    <span class="quick_text"> ToDo Manager </span>
                                    <span class="quick_newimg"></span>
                                </li>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.chattrack"  class="quick_ancor">
                                <li class="quick_menu_items">
                                    <i class="fa fa-play quick_play" aria-hidden="true"></i>
                                    <span class="quick_text"> Chat Track </span>
                                    <span class="quick_newimg"></span>
                                </li>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.tibetinnovations.storibud"  class="quick_ancor">
                                <li class="quick_menu_items">
                                    <i class="fa fa-play quick_play" aria-hidden="true"></i>
                                    <span class="quick_text"> Storibud </span>
                                    <span class="quick_newimg"></span>
                                </li>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.sachtech.avtar.learningabc_free&hl=en"  class="quick_ancor">
                                <li class="quick_menu_items">
                                    <i class="fa fa-play quick_play" aria-hidden="true"></i>
                                    <span class="quick_text"> Learning ABC </span>
                                    <span class="quick_newimg"></span>
                                </li>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.sachtech.nardeepsandhu.fitnessapp"  class="quick_ancor">
                                <li class="quick_menu_items">
                                    <i class="fa fa-play quick_play" aria-hidden="true"></i>
                                    <span class="quick_text"> FinelyFit </span>
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
            <!--quick links end here-->
            <div class="col-md-6">
                <div class="row news_box">
                    <h1 class="whats_new_heading">
                        <i class="fa fa-dot-circle-o"></i>
                        <span class="whats_heading">What's New</span>
                    </h1>

                    <div class="whats_new_box">
                        <ul class="whats_new_ul">
                            <a href="career?job=career_php" class="quick_ancor">
                                <li class="whats_menu_items">
                                    <i class="fa fa-arrow-right whats_arrow" aria-hidden="true"></i>
                                    <span class="whats_text"> Job Opening for Web Developer</span>
                                    <span class="whats_newimg"></span>
                                </li>
                            </a>
                            <a href="career?job=career_graphic_designer" class="quick_ancor">
                                <li class="whats_menu_items">
                                    <i class="fa fa-arrow-right whats_arrow" aria-hidden="true"></i>
                                    <span class="whats_text"> Job Opening for Graphics Designer </span>
                                    <span class="whats_newimg"></span>
                                </li>
                            </a>
                            <a href="career?job=career_seo" class="quick_ancor">
                                <li class="whats_menu_items">
                                    <i class="fa fa-arrow-right whats_arrow" aria-hidden="true"></i>
                                    <span class="whats_text">  Job Opening for SEO </span>
                                    <span class="whats_newimg"></span>
                                </li>
                            </a>
                            <a class="quick_ancor">
                                <li class="whats_menu_items">
                                    <i class="fa fa-arrow-right whats_arrow" aria-hidden="true"></i>
                                    <span class="whats_text"> Expanding our domain in Manufacturing sector </span>
                                    <span class="whats_newimg"></span>
                                </li>
                            </a>
                            <a href="skill" class="quick_ancor">
                                <li class="whats_menu_items">
                                    <i class="fa fa-arrow-right whats_arrow" aria-hidden="true"></i>
                                    <span class="whats_text"> Skill Development  </span>
                                    <span class="whats_newimg"></span>
                                </li>
                            </a>
                            <a href="career?job=career_android" class="quick_ancor">
                                <li class="whats_menu_items job_opening" id="career_android">
                                    <i class="fa fa-arrow-right whats_arrow" aria-hidden="true"></i>
                                    <span class="whats_text"> Job Opening for Mobile App Developer </span>
                                    <span class="whats_newimg"></span>
                                </li>
                            </a>
                            <a  class="quick_ancor">
                                <li class="whats_menu_items">
                                    <i class="fa fa-arrow-right whats_arrow" aria-hidden="true"></i>
                                    <span class="whats_text"> One step forward to digital india </span>
                                    <span class="whats_newimg"></span>
                                </li>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=kapilsony.sachtech.com.todomanager" class="quick_ancor">
                                <li class="whats_menu_items">
                                    <i class="fa fa-arrow-right whats_arrow" aria-hidden="true"></i>
                                    <span class="whats_text"> Launched ToDo Manager App </span>
                                    <span class="whats_newimg"></span>
                                </li>
                            </a>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row news_box banner_image_box">
                    <img src="images/shutterstock_348899993.jpg" class="banner_image img-responsive">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="clear"></div>
<!--quick links end here-->

<!-- career start here -->
<div class="col-md-12 career_box nogutter">
<!-- curve image -->
    <div class="svg-element-careers-top"> <img src="images/top-new.png" alt="curve-image"></div>

    <div class="row career_box_row">
        <div class="col-xs-12 col-md-6 career_leftside">
            <div class="row career_leftinner">
                <h2 class="career_text_heading">Careers</h2>
                <p class="index_career_desc"> Great career Opportunities.</p>
                <p class="index_career_desc"> Great people.</p>
                <p class="career_learn">
                    <a data-scrollreveal="enter from the top wait 0.17s" href="career" data-scrollreveal-initialized="true" data-scrollreveal-complete="true" class="career_learn_a">Learn more</a>
                </p>
            </div>

        </div>
        <span class="overlay"></span>
        <span class="overlay1"></span>
        <span class="overlay2"></span>
        <div class="col-xs-12 col-md-6 career_rightside">

            <div class="row career_right_inner">
                <ul class="career_right_ul">
                    <a href="about?value" class="index_careeer_ancor">
                        <li class="career_right_li">
                            <div class="each-career">
                                <span class="career_text">
                                    Why Work With Us ?
                                </span>
                                <i class="fa fa-2x fa-external-link pull-right external-link" aria-hidden="true"></i>
                            </div>
                        </li>
                    </a>
                    <a href="career" class="index_careeer_ancor">
                        <li class="career_right_li">
                             <div class="each-career">
                                <span class="career_text">
                                    View our Careers
                                </span>
                                <i class="fa fa-2x fa-external-link pull-right external-link" aria-hidden="true"></i>
                            </div>
                        </li>
                    </a>
                    <a href="services?service=serv-refrd-sachtech" class="index_careeer_ancor">
                        <li class="career_right_li">
                             <div class="each-career">
                                <span class="career_text">
                                    Get referred to SachTech
                                </span>
                                <i class="fa fa-2x fa-external-link pull-right external-link" aria-hidden="true"></i>
                            </div>
                        </li>
                    </a>
                    <a href="faq?qry=faq-online-reg" class="index_careeer_ancor">
                        <li class="career_right_li">
                             <div class="each-career">
                                <span class="career_text">
                                    Online Registration
                                </span>
                                <i class="fa fa-2x fa-external-link pull-right external-link" aria-hidden="true"></i>
                            </div>
                        </li>
                    </a>
                    <a href="career?scrl=career-news-room" class="index_careeer_ancor">
                        <li class="career_right_li">
                            <div class="each-career">
                            <span class="career_text">
                                News Room
                            </span>
                                <i class="fa fa-2x fa-external-link pull-right external-link" aria-hidden="true"></i>
                            </div>
                        </li>
                    </a>
                    <a href="contact" class="index_careeer_ancor">
                        <li class="career_right_li">
                             <div class="each-career">
                                <span class="career_text">
                                   Join Us
                                </span>
                                <i class="fa fa-2x fa-external-link pull-right external-link" aria-hidden="true"></i>
                            </div>
                        </li>
                    </a>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="svg-element-careers-bottom carrer_bottom"></div>
</div>
<div class="clear"></div>
<!-- career end here -->

<!-- testimonial start here -->
<div class="col-md-12 testimonial_box1">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h2 class="testimonial_heading headings"> TESTIMONIAL</h2>
        <div class="row testimonial_box">
            <div id="myCarousel" class="carousel slide testimonial_slider" data-ride="carousel">
                
                <div class="carousel-inner">
                      <div class="item active">
                            <div class="testi_text_box">
                                <h4 class="test_heading">They have strong technical expertise and attention to detail was tremendously valuable on my project.
                                    They are very professional and a very easy to work with approach. They work on weekends and understand the client needs very well. They quickly understood the objectives and priorities of my project. The quality of their work is recognized and appreciated.
                                </h4>
                                <p class="testimonial_footer">R Reddy</p>
                            </div>
                      </div>

                      <div class="item">
                            <div class="testi_text_box">
                                <h4 class="test_heading">
                                    I just wanted to thank you for the excellent job you have done on our website. I am very pleased with the final product. Great Experience to work  with SachTech Solution Team and definitely i will recommended to my friends and also will start new software with you
                                </h4>
                                <p class="testimonial_footer">H Raza</p>
                            </div>
                      </div>
                    
                      <div class="item">
                            <div class="testi_text_box">
                                <h4 class="test_heading">
                                    Best Software Company in India with whom i have worked till now and  is connected with SachTech Solution from last three years &  will looking for having more work with them in future. Strong Team with Strong Technical Management. All the Best Team SachTech Solution.
                                </h4>
                                <p class="testimonial_footer">Worldwide Business Solution llc</p>
                            </div>
                      </div>
                      <div class="item">
                            <div class="testi_text_box">
                                <h4 class="test_heading">
                                    Great Work Ethics. Very professional and devoted.
                                    Very flexible and hardworking.
                                    I highly recommend SachTech Solution.
                                </h4>
                                <p class="testimonial_footer">Jarios Oslimo</p>
                            </div>
                      </div>
                    <div class="item">
                            <div class="testi_text_box">
                                <h4 class="test_heading">
                                    SachTech Solution provided the best service i could have ever ask for. From the professional quality of work, to the humble amount of budget offered at us. Mr Kapil and his team did their best and with full responsibility completing the task that was given to them. Totally recommended
                                </h4>
                                <p class="testimonial_footer">Jilvan Junior</p>
                            </div>
                      </div>
                    <div class="item">
                        <div class="testi_text_box">
                            <h4 class="test_heading">
                                Excellent worker and did an awesome job. Went above and beyond expectations! Easy to work with and very responsive. Will hire again.
                            </h4>
                            <p class="testimonial_footer">Mohamad Firdaus</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testi_text_box">
                            <h4 class="test_heading">
                                It was great working with SachTech Team. They are really a hard worker who complete the project on time.
                            </h4>
                            <p class="testimonial_footer">T. Thiru</p>
                        </div>
                    </div>

                </div>
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                  <li data-target="#myCarousel" data-slide-to="3"></li>
                  <li data-target="#myCarousel" data-slide-to="4"></li>
                  <li data-target="#myCarousel" data-slide-to="5"></li>
                  <li data-target="#myCarousel" data-slide-to="6"></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
<!-- testimonial start here -->


<?php
    include_once 'footer.php';
?>
