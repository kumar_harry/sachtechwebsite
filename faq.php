<?php
include_once 'header.php';
?>
<!-- faq start here-->
<div class="col-md-12">
    <div class="col-md-1"></div>
    <div class="col-xs-12 col-md-10">
        <div class="row">
            <div class="career_top_heading_box">
                <h4 class="career_top_heading">Frequently Asked Questions</h4>
                <hr>
                <div class="container-fluid faq-inner-main-box">
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question  </span>
                            <span> : </span>
                            <span class="faq-question-text"> What programming language has the best tooling for the modern developer? Why?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">For Web development: You need to know HTML, CSS and JavaScript languages. That's the bare minimum to get started. As for frameworks, there are hundreds of them, but you only need couple of them. For tools, like development tools,
                                you only need notepad and a browser. Of course there are development tools that will help you more
                                efficient like brackets.
                                <br>
                                <span class="faq_points_red"> For Mobile apps: </span>
                                <br>
                                You can just stick with HTML, CSS and JavaScript to build mobile apps using Cordova.
                                For native development you will need to know these languages:
                                Java for android within Android Studio
                                Swift for iOS within xcode C# for Windows Phone within Visual Studio.
                                <br><br>
                                <span class="faq_points_red"> For desktop apps:</span>
                                <br>
                                You have numerous options, you can nearly use any programming language:
                                C# with WPF or Windows Forms
                                C++ with Qt, WPF, or wxwidgets
                                Python with tkinter, wxpython, pyqt, or kivy
                                Perl with perl-tk Webapp that you use as desktop app.
                                <br><br>
                                <span class="faq_points_red"> For cross-platform apps:</span>
                                <br>
                                Qt, Xamarin

                                <br><br>
                                <span class="faq_points_red"> Integrated development environments (IDE):</span>
                                Check Jetbrains for their tools &nbsp; ,
                                Visual studio from microsoft &nbsp; ,
                                Xcode &nbsp; ,
                                Eclipse &nbsp; ,
                                Netbeans &nbsp; ,
                                Aptana.
                                <br>
                                <span class="faq_points_red"> Tools:</span>
                                Notepad++ on windows Vim\Emacs on linux &nbsp; ,
                                Command line knowledge will be needed &nbsp; ,
                                Brackets for web development There are countless number of languages, frameworks, and tools.
                        </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">What is best modern programming language?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">In 2017, and let’s cover it – having coding skills makes a
                                difference, it doesn't matter what job you have got. It’s not only for coders or programmers
                                anymore, and it’s more important than ever that everybody have at the least a basic knowledge of coding skills.
                            <br>
                                Popularity of programming languages utilizing the variety of expert engineers and search engine rankings.
                                <br>
                                The results :
                                <br>
                                Java,
                                C,
                                C++,
                                C#,
                                Python,
                                PHP,
                                .NET,
                                JavaScript,
                                Assembly Language,
                                Ruby,
                                Perl,
                                Delphi,
                                VisualBasic,
                                Swift,
                                MATLAB,
                                Pascal,
                                Groovy,
                                Objective-C,
                                R, &nbsp;
                                PL/SQL .
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">  I want to create a social network iOS app. I learned the
                                Swift language. What should I use for my backend?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text"> We will recommended you Firebase because Firebase features
                                are the Realtime Database, Storage, Hosting, and Test Lab.
                            <br>
                                You should have no worries about Firebase’s performance, since it is powered by Google’s
                                infrastructure.Firebase’s learning curve is minimal. The documentation is elaborate enough to
                                take you through step by step processes of applying its functionalities.
                            </span>
                        </p>
                    </div>


                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text"> I want to create android app for this can SachTech Solution create this
                                and How much time it would take to develop?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text"> Yes we can create android app and Calculating the specific project timeframe is
                                not possible without the definite understanding of the future app’s structure & design.
                                Development time for Android app : -
                            <br>
                                <span class="faq_points_red">Simple Android app (up to 4 weeks)</span>
                                <br>
                                API Integration,
                                Simple UI/UX,
                                Property list,
                                Map view,
                                Simple property view,
                                Simple Search. &nbsp;
                                <br><br>
                                <span class="faq_points_red"> Middle Android app( up to 8-12 weeks)</span>
                                <br>
                                UI/UX- Tablets support,
                                Search by map,
                                Social Integration,
                                Detail property view,
                                Several Roles,
                                Filters.
                                <br><br>
                                <span class="faq_points_red">Complex Android app( over 16 weeks)</span>
                                <br>
                                Difficult,
                                Integration with 3rd Party services,
                                Custom Animation,
                                Advanced Filters,
                                Payments Integration,
                                Chat feature.
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">
                                What facilities do you provide in SachTech Solution Skill Development?
                            </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">
                                Dedicated experienced developers will be assigned to the particular groups, making concept clear to them by linking it with real life examples along with
                                internet facilities will be provided to them for their own R&D on the technology, PD classes for making them facing the future interviews 100% assistance for the job
                                after particular completion of whole course.
                            </span>
                        </p>
                    </div>

                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text"> My son is an average student and his aggregate marks is less than 60% in the B. Tech exam, can he join SACHTECH SOLUTION SKILL DEVELOPMENT?
                            <br>
                                Is there any discrimination on basis of percentage at industrial level?
                            </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">Yes, he can join the SachTech Solution Skill Development. NO there is no kind of discrimination on the basis of percentage as we are having our own methodology of analyzing
                                candidate skills at SachTech Solution (SachTech Solution Skill Development).
                            </span>
                        </p>
                    </div>


                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text"> I am student of 2nd year engineering can I join SACHTECH SOLUTION
                                SKILL DEVELOPMENT? How it will be useful?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text"> Yes, you can join it for the period of 6/8 weeks. It will be useful to learn the basics of any particular language & to know about the IT corporates work culture
                                 & their method of working & accordingly prepare yourself for facing future interview with ease.
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <a class="faq-online-reg"></a>
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">What is the process to take admission through online mode?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">
                                For online admissions related query consult to the below contact persons:<br>
                                Mr. Diwanshu Tangri : <span class="faq_points_red">+91-9780131743</span><br>
                                Ms. Neha Garg : <span class="faq_points_red">+91-7087425288</span><br>
                                We will provide online payment link as soon as possible.
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">What is the mode of Payment to join SACHTECH SOLUTION SKILL DEVELOPMENT?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">
                                One can pay by visiting company by cash or by doing the swiping of their debit or credit cards,
                                by Paytm, by doing NEFT, Net Banking, Transfer to company account by visiting any nearby bank.
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text"> Can I deposit my fees in Installment?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">
                               If their is any candidate who is willing to learn & explore his/her skills but is not able to do because of financial issues then after listening to
                                his/her problem, he/she can pay his/her fees in installments after having permission from the
                                company management.
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">I took admission, but then I never attended a single class,
                                I want to get my amount refunded?
                        </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">
                                Fees is not refundable at any circumstances nor transmittable in other candidate account.
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">
                                What is the process to take admission through online mode?
                            </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">
                                <!--Yes you can by clicking on the link mentioned........................... For further help you can call to
                                our Business Team Professionals.-->
                                For online admissions related query consult to the below contact persons:
                                <br>
                                Mr. Diwanshu Tangri : <span class="faq_points_red">  +91-7087972568 </span> <br>
                                Ms. Neha Garg<span class="faq_points_red">   : +91-7087425288</span>
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">
                                Do you provide any Demo classes?
                            </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer</span>
                            <span> : </span>
                            <span class="faq-answer-text">
                                Yes at the time of visit we arrange demo classes to the candidates so as to know about the particular developer brushing candidates technical skills methodology,
                                curriculum covered during course, future benefit of the applied technology & reason for joining
                                the particular technology course.
                            </span>
                        </p>
                    </div>
                    <div class="faq-ques-content-box">
                        <p class="faq-quest-p">
                            <span class="faq-question-word">Question </span>
                            <span> : </span>
                            <span class="faq-question-text">
                                Can I swipe the card?
                            </span>
                        </p>
                        <p class="faq-answer-p">
                            <span class="faq-answer-word">Answer </span>
                            <span> : </span>
                            <span class="faq-answer-text">
                                Yes.
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<!-- faq end here-->
<?php
include_once 'footer.php';
?>
<script>
    var urlData = window.location.href;
    if((urlData.indexOf("qry=")) > 0)
    {
        urlData = urlData.split("qry=");
        $('html, body').animate({
            scrollTop: $("."+urlData[1]).offset().top
        }, 800);
    }
</script>

