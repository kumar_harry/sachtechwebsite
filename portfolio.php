<?php include("header.php"); ?>
    <!----------------------------------------------------------------------------------------->
    <!-- portfolio header start here -->
    <!----------------------------------------------------------------------------------------->

    <div class="col-md-12 portfoilio-header-main12 nogutter">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <!-- about us part start here -->
            <div class="row portfoilio-header-main">
                <h1 class="port-heading">
                    PORTFOLIO
                </h1>
            </div>
            <!-- about us part end here -->
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="clear"></div>
    <!----------------------------------------------------------------------------------------->
    <!-- portfolio header start here -->
    <!----------------------------------------------------------------------------------------->

    <!----------------------------------------------------------------------------------------->
    <!-- portfolio content start here -->
    <!----------------------------------------------------------------------------------------->

    <div class="col-md-12 portfoilio-content-main12 nogutter">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <!--portfolio selection box end here-->
            <div class="row portfolio-select">
                <div class="col-md-4">
                    <h1 class="port-type-text"> All </h1>
                </div>
                <div class="col-md-2 col-md-offset-6 selectport-box">
                    <div class="dropdown">
                        <button class="btn btn-danger dropdown-toggle sort-portf" type="button" data-toggle="dropdown">
                            SORT PORTFOLIO
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li class="port-type" id="port-all"><a href="#"> All </a></li>
                            <li class="divider"></li>
                            <li class="port-type" id="port-web-develop"><a href="#"> WEB DEVELOPMENT </a></li>
                            <li class="port-type" id="port-mobile-app"><a href="#"> ANDROID APPLICATION </a></li>
                            <li class="port-type" id="port-ios"><a href="#"> iOS DEVELOPMENT </a></li>
                            <li class="port-type" id="port-logo-design"><a href="#"> LOGO DESIGN </a></li>
                            <li class="port-type" id="port-digital-mark"><a href="#"> DIGITAL MARKETING </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <!--    portfolio selection box end here       -->
            <!--    portfolio project slider box start here-->

            <!--web portfolio start here-->
            <div id="web-port-design-content">
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/third.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Disthi</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/second.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/fourth.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/fifth.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/fourth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/Home.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/fifth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/Home_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/login.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/login_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Finleyfit</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/get%20look/download1.PNG"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/get%20look/download1_imac2015_front.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/get%20look/home.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/get%20look/home_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/get%20look/offer.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/get%20look/offer_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">GetLook</h4>
                        </div>
                    </div>
                </div>

                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/ply%20bazar%20website/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/ply%20bazar%20website/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/ply%20bazar%20website/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/ply%20bazar%20website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/ply%20bazar%20website/third.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/ply%20bazar%20website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Ply Bazar</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Learning%20ABC%20Website/Learnin ABC Website_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Learning%20ABC%20Website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Learning ABC</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/no%20walk%20keaton/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/no%20walk%20keaton/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/no%20walk%20keaton/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/no%20walk%20keaton/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/no%20walk%20keaton/third.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/no%20walk%20keaton/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">No Walk</h4>
                        </div>
                    </div>
                </div>

                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/third%20-%20Copy.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/fourth.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/fourth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/sixth.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/fifth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/sixth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Royal%20Munch%20Website/fifth%20-%20Copy.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Royal Munch</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/third.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/fourth.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/fourth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/fifth.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/fifth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">STS Mentor</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/size%20usa/first.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/size%20usa/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Size Usa</h4>
                        </div>
                    </div>
                </div>

                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Scan2Tailor/first.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Scan2Tailor/second.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Scan2Tailor/third.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Scan2Tailor/fourth.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Scan2Tailor</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Txparts%20Website/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Txparts%20Website/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Txparts%20Website/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Txparts%20Website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Txparts</h4>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/scan%20to%20fit%20website/Scan2Fit_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/scan%20to%20fit%20website/Scan2Fit.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/scan%20to%20fit%20website/secondd.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/scan%20to%20fit%20website/secondd_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/scan%20to%20fit%20website/third.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/scan%20to%20fit%20website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Scan2Fit</h4>
                        </div>
                    </div>
                </div>

                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Friend%20Tracker/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Friend%20Tracker/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Friend%20Tracker/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Friend%20Tracker/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Friend Tracker</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Spinner%20Force/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Spinner%20Force/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Spinner%20Force/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Spinner%20Force/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Spinner%20Force/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Spinner%20Force/third.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Spinner Force</h4>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Swift/first.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Swift/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Swift/second.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Swift/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Swift</h4>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <!--web portfolio end here-->

            <!--android portfolio start here-->
            <div id="android-port-design-content">
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Appointment%20Buddy/IMG_29072017_143518_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Appointment%20Buddy/IMG_29072017_143630_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Appointment%20Buddy/IMG_29072017_143658_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Appointment%20Buddy/IMG_29072017_143718_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Appointment Buddy</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/1.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/Screenshot_2016-08-28-23-01-50.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/2.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/Screenshot_2016-08-28-23-01-59.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/3.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/Screenshot_2016-08-28-23-02-06.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/4.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/Screenshot_2016-08-28-23-02-34.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/5.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/Screenshot_2016-08-28-23-02-48.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/6.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/Screenshot_2016-09-12-18-24-42.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/7.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/CFH%20APP/Screenshot_2016-09-12-18-25-22.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">CFH</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/ChatTrack%20APP/IMG_29072017_140220_0.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/ChatTrack%20APP/IMG_29072017_140332_0.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/ChatTrack%20APP/IMG_29072017_140432_0.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/ChatTrack%20APP/IMG_29072017_140825_0.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/ChatTrack%20APP/IMG_29072017_140907_0.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">ChatTrack</h4>
                        </div>
                    </div>
                </div>

                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/JustFor%20Women/1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/JustFor%20Women/1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/JustFor%20Women/2.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/JustFor%20Women/3.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/JustFor%20Women/4.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/JustFor%20Women/5.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">JustFor Women</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141754_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_142003_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_142003_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_142003_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141928_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141928_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141928_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141859_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141859_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141859_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141812_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141812_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141812_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141754_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/Learning%20ABC%20APP/IMG_29072017_141754_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Learning ABC App</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142601_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142601_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142601_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142631_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142631_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142631_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142601_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142713_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142713_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142738_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142738_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142738_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142713_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142820_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142820_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142820_0_nexus4_portrait.png "
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142846_0_nexus4_angle1.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142846_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142601_0_nexus4_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/storibud/IMG_29072017_142846_0_nexus4_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Storibud</h4>
                        </div>
                    </div>
                </div>
                <div class="row port_slider-row">


                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-11-59-04_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-11-59-04_com.example.nardeepsandhu.fitnessapp_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-11-59-04_com.example.nardeepsandhu.fitnessapp_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-11-59-08_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-11-59-08_com.example.nardeepsandhu.fitnessapp_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-11-59-08_com.example.nardeepsandhu.fitnessapp_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-02-45_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-02-45_com.example.nardeepsandhu.fitnessapp_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-02-45_com.example.nardeepsandhu.fitnessapp_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-03-22_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-03-22_com.example.nardeepsandhu.fitnessapp_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-03-22_com.example.nardeepsandhu.fitnessapp_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-06-01_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-06-01_com.example.nardeepsandhu.fitnessapp_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-06-01_com.example.nardeepsandhu.fitnessapp_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-07-11_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-07-11_com.example.nardeepsandhu.fitnessapp_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-07-11_com.example.nardeepsandhu.fitnessapp_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-14-53_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-11-59-04_com.example.nardeepsandhu.fitnessapp_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-14-53_com.example.nardeepsandhu.fitnessapp_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/finelyfit/Screenshot_2016-06-23-12-14-53_com.example.nardeepsandhu.fitnessapp_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Finelyfit</h4>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141227_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141227_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>

                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141227_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141320_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141320_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141320_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141350_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141350_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141350_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141418_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141418_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/expense%20manager/IMG_29072017_141418_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Expense Manager</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_144908_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_144908_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_144908_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_144951_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_144951_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_144951_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_145129_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_145129_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_145129_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/friendtracker/IMG_29072017_144908_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Friend Tracker</h4>
                        </div>
                    </div>
                </div>

                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/1Home_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/1Home_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/1Home_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/2_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/2_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/3_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/2_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/3_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/3_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/4_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/4_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/4_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/5_nexus4_angle1.png  "
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/5_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/2_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/5_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/11_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/11_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/royalmunch/11_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Royalmunch</h4>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/trump%20your%20self/IMG_29072017_141533_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/trump%20your%20self/IMG_29072017_141533_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/trump%20your%20self/IMG_29072017_141533_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/trump%20your%20self/IMG_29072017_141620_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/trump%20your%20self/IMG_29072017_141620_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/trump%20your%20self/IMG_29072017_141620_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/trump%20your%20self/IMG_29072017_141647_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/trump%20your%20self/IMG_29072017_141647_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Trump Your Self</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide " data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142310_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142310_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142310_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142359_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142359_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142359_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142424_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142424_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142424_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142448_0_nexus4_angle1.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142448_0_nexus4_landscape.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/android/txparts/IMG_29072017_142448_0_nexus4_portrait.png"
                                                 class="projSlider "/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Txparts</h4>
                        </div>
                    </div>
                </div>


            </div>
            <!--android portfolio end  here-->
            <!--ios portfolio start  here-->
            <div id="ios-port-design-content">
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Fitnessapp/1.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Fitnessapp/2.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Fitnessapp/3.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Fitnessapp/4.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Fitnessapp/5.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Fitnessapp/6.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Fitnessapp/Simulator%20Screen%20Shot%20Jul%2028,%202017,%207.14.14%20PM.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Fitnessapp/5.png" class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Fitness App</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Make%20Bet/make_bets_1_no_bets.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Make%20Bet/make_bets_2_editing_bets.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Make%20Bet/make_bets_3_adding_parlay_bet.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Make%20Bet/make_bets_4_selecting_team.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Make%20Bet/make_bets_5_scrolling_to_other_teams.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Make%20Bet/make_bets_6_finished_creating_bets.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Make%20Bet/make_bets_7_viewing_the_line.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Make Bet</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Learning%20ABC%20APP/IMG_29072017_142003_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Learning%20ABC%20APP/IMG_29072017_141928_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Learning%20ABC%20APP/IMG_29072017_141859_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Learning%20ABC%20APP/IMG_29072017_141812_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Learning%20ABC%20APP/IMG_29072017_141754_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Learning ABC App</h4>
                        </div>
                    </div>
                </div>

                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">

                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Friend%20Tracker/IMG_29072017_144908_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Friend%20Tracker/IMG_29072017_144951_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Friend%20Tracker/IMG_29072017_145129_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Friend Tracker</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(1)_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(1)_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(2)_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(3)_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(4)_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>

                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(1)_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(2)_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(3)_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(4)_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(1).jpeg "
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(2).jpeg"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(3).jpeg"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/getlook/screen696x696%20(4).jpeg"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">GetLook</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/1_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/1_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/2_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/2_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/3_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/3_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/4_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/1_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/4_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/5_iphone7plusrosegold_landscape.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/mobigym/5_iphone7plusrosegold_portrait.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Mobigym</h4>
                        </div>
                    </div>
                </div>

                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">

                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Personal%20Expense%20Manger/IMG_29072017_141227_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Personal%20Expense%20Manger/IMG_29072017_141320_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Personal%20Expense%20Manger/IMG_29072017_141350_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Personal%20Expense%20Manger/IMG_29072017_141418_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Expense Manager</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Storibud/IMG_29072017_142601_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Storibud/IMG_29072017_142631_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Storibud/IMG_29072017_142713_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Storibud/IMG_29072017_142738_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Storibud/IMG_29072017_142820_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Storibud/IMG_29072017_142846_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Storibud</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Trump%20YourSelf/IMG_29072017_141533_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Trump%20YourSelf/IMG_29072017_141620_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Trump%20YourSelf/IMG_29072017_141647_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Trump Your Self </h4>
                        </div>
                    </div>
                </div>
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">

                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Tx%20Parts/IMG_29072017_142310_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Tx%20Parts/IMG_29072017_142359_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Tx%20Parts/IMG_29072017_142424_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/Tx%20Parts/IMG_29072017_142448_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Tx Parts</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/We%20Tube/IMG_29072017_142055_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/ios/We%20Tube/IMG_29072017_142148_0.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">We Tube</h4>
                        </div>
                    </div>
                </div>
            </div>
            <!--ios portfolio end  here-->
            <!--logo design portfolio start  here-->
            <!--<div id="logo-port-design-content">
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/App-Icon_FinelyFit.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_142930_01.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_142951_02.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Finelyfit</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143013_03.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143714_04.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143734_05.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Expense Manager</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143753_06.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143816_07.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143839_08.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Sts App</h4>
                        </div>
                    </div>
                </div>
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143859_09.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143919_010.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_143949_011.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">To Do Manager</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_144018_013.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_144109_014.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/IMG_31072017_144149_015.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Yagoo Face</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/logo_1024.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/Logo_CFH.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/logo_runnershigh.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Make Body</h4>
                        </div>
                    </div>
                </div>
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/shutterstock_348899993.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/logo%20design.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/stslogo.png" class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Diet Dairy</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/1.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/2r.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/3tr.jpg" class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Text Generator</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/4.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/5.png" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/logo%20design/6.png" class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Stori Bud</h4>
                        </div>
                    </div>
                </div>
            </div>-->
            <div id="logo-port-design-content">
                <div class="row port_slider-row">
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img src="images/training/logo%20design/App-Icon_FinelyFit.png"
                                 class="projSlider"/>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Finelyfit</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img src="images/training/logo%20design/logo_1024.png" class="projSlider"/>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Make Body</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img src="images/training/logo%20design/1.png" class="projSlider"/>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Text Generator</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_143753_06.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Sts App</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_143839_08.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Friend Tracker</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_143734_05.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Feeding India</h4>
                        </div>
                    </div>
                </div>
                <div class="row port_slider-row">
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_143919_010.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Tx Parts</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_143013_03.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">SachTech</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/stslogo.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">STS Mentor</h4>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_143949_011.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Learning ABC</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/look_red.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">GetLook</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_143859_09.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">ToDo Manager</h4>
                        </div>
                    </div>
                </div>
                <div class="row port_slider-row">
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/logo%20design.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Ply Bazar</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_144109_014.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Stori Bud</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/Logo_CFH.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">CFH</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_144149_015.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Scan2Fit</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/IMG_31072017_142951_02.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Friend Finder</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row proj-slider-row">
                            <img class="projSlider" src="images/training/logo%20design/logo_runnershigh.png">
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Runners High</h4>
                        </div>
                    </div>
                </div>

            </div>
            <!--logo design portfolio end  here -->

            <!--digital Marketing design portfolio start  here-->
            <div id="digital-port-design-content">
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/second.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item " align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/fourth.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/fifth.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/fourth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/Home.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/fifth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/Home_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/login.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Finleyfit%20Website/login_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Finleyfit</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Learning%20ABC%20Website/Learnin ABC Website_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Learning%20ABC%20Website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Learning ABC</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/third.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/disthi%20website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Disthi</h4>
                        </div>
                    </div>
                </div>
                <div class="row port_slider-row">
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/third.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/third_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/fourth.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/fourth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/fifth.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/sts%20mentor%20website/fifth_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">STS Mentor</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Scan2Tailor/first.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Scan2Tailor/second.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Scan2Tailor/third.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Scan2Tailor/fourth.PNG" class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Scan2Tailor</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row proj-slider-row">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Friend%20Tracker/first.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Friend%20Tracker/first_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Friend%20Tracker/second.PNG"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                    <div class="item" align="center">
                                        <a class="reviewAncor" href="#">
                                            <img src="images/training/web/Friend%20Tracker/second_imac2015_front.png"
                                                 class="projSlider"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row proj-row">
                            <h4 class="proj-name-text">Friend Tracker</h4>
                        </div>
                    </div>
                </div>
            </div>
            <!--digital Marketing portfolio end  here -->


            <!--portfolio project slider box end here-->


        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="clear"></div>

    <!----------------------------------------------------------------------------------------->
    <!-- portfolio header end here -->
    <!----------------------------------------------------------------------------------------->
<?php include("footer.php"); ?>
<script>
    var url = window.location.href;
    if(url.indexOf("?")>0){
        url = url.split("?")[1];
        if(url == "ios"){
            $(".port-type-text").html("iOS Application");
            $("#web-port-design-content").fadeOut(500);
            $("#android-port-design-content").fadeOut(500);
            $("#ios-port-design-content").fadeIn(500);
            $("#logo-port-design-content").fadeOut(500);
            $("#digital-port-design-content").fadeOut(500);
        }else if(url == "android"){
            $(".port-type-text").html("Mobile Application");
            $("#web-port-design-content").fadeOut(500);
            $("#android-port-design-content").fadeIn(500);
            $("#ios-port-design-content").fadeOut(500);
            $("#logo-port-design-content").fadeOut(500);
            $("#digital-port-design-content").fadeOut(500);
        }else{
            $(".port-type-text").html("All");
            $("#web-port-design-content").fadeIn(500);
            $("#android-port-design-content").fadeIn(500);
            $("#ios-port-design-content").fadeIn(500);
            $("#logo-port-design-content").fadeIn(500);
            $("#digital-port-design-content").fadeIn(500);
        }
    }
</script>
