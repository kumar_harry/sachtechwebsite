<?php
session_start();
require_once('Classes/PURPOSAL.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$purposalClass = new \Classes\PURPOSAL();
$requiredfields = array('type');
($response = RequiredFields($_REQUEST, $requiredfields));
if($response['Status'] == 'Failure'){
    $purposalClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_REQUEST['type'];
if($type == "send_purposal")
{
    $requiredfields = array('client_name','client_email','client_contact','client_budget','client_country',
        'client_description');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $purposalClass->apiResponse($response);
        return false;
    }
    $client_name    =   $_REQUEST['client_name'];
    $client_email   =   $_REQUEST['client_email'];
    $client_contact =   $_REQUEST['client_contact'];
    $client_budget  =   $_REQUEST['client_budget'];
    $client_country =   $_REQUEST['client_country'];
    $client_social_id   =   $_REQUEST['client_social_id'];
    $client_description =   $_REQUEST['client_description'];

    $response = $purposalClass->sendPurposalMail($client_name,$client_email,$client_contact,$client_budget,$client_country,
        $client_social_id,$client_description);
    $purposalClass->apiResponse($response);
}
/*send mail for career apply*/

else if($type == "careerApplyMail"){
    $requiredfields = array('car_app_name','car_app_email','car_app_phone','car_app_job_type');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $purposalClass->apiResponse($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $file_name = $_FILES['career_attachFile']['name'];
    $file_size = $_FILES['career_attachFile']['size'];
    $file_tmp = $_FILES['career_attachFile'] ['tmp_name'];
    $file_ext = strtolower(end(explode('.', $_FILES['career_attachFile']['name'])));
    $expensions = array("doc", "docx", "pdf");
    $file_url = "";
    if (in_array($file_ext, $expensions) === false) {
        $status = error;
        $message = "extension not allowed, please choose a DOCX,DOC,PDF file.";
    } else {
        $file_name = "File" . rand(000000, 999999) . "." . $file_ext;
        move_uploaded_file($file_tmp, "Files/files/$file_name");
        $file_url = "http://localhost:81/sachtechwebsite/webservice/Files/files/$file_name";
    }
    $car_app_name       =   $_REQUEST['car_app_name'];
    $car_app_email      =   $_REQUEST['car_app_email'];
    $car_app_phone      =   $_REQUEST['car_app_phone'];
    $car_app_job_type   =   $_REQUEST['car_app_job_type'];
    $response = $purposalClass->careerApplyMail($car_app_name,$car_app_email,$car_app_phone,$car_app_job_type,$file_url,$file_name);
    $purposalClass->apiResponse($response);
}
else if($type == "contactus")
{
    $requiredfields = array('contact_name','contact_email','contact_number','contact_message');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $purposalClass->apiResponse($response);
        return false;
    }
    $contact_name      =   $_REQUEST['contact_name'];
    $contact_email     =   $_REQUEST['contact_email'];
    $contact_number    =   $_REQUEST['contact_number'];
    $contact_message   =   $_REQUEST['contact_message'];
    $response = $purposalClass->contactUsMail($contact_name,$contact_email,$contact_number,$contact_message);
    $purposalClass->apiResponse($response);
}
else if($type == "paymentUser"){
    $requiredfields = array('firstname','lastname','courses','city','address','state','country','zip','phone','email','amount');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $purposalClass->apiResponse($response);
        return false;
    }
    $firstname      =   $_REQUEST['firstname'];
    $lastname       =   $_REQUEST['lastname'];
    $courses        =   $_REQUEST['courses'];
    $city           =   $_REQUEST['city'];
    $address        =   $_REQUEST['address'];
    $state          =   $_REQUEST['state'];
    $country        =   $_REQUEST['country'];
    $zip            =   $_REQUEST['zip'];
    $phone          =   $_REQUEST['phone'];
    $email          =   $_REQUEST['email'];
    $amount          =   $_REQUEST['amount'];
    $response       = $purposalClass->paymentUser($firstname,$lastname,$courses,$city,$address,$state,$country,$zip,$phone,$email,$amount);
    $purposalClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $orderClass->apiResponse($response);
}
?>