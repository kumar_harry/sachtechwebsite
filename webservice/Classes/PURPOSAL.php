<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 8/1/2017
 * Time: 1:55 PM
 */

namespace Classes;

require_once('CONNECT.php');
require('SMTP/PHPMailerAutoload.php');

class PURPOSAL
{
    public $link = null;

    function __construct()
    {
        $this->link = new CONNECT();
        /*$this->userClass = new USERCLASS();*/
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }

    public function sendPurposalMail($client_name, $client_email, $client_contact, $client_budget, $client_country,
                                     $client_social_id, $client_description)
    {
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'a2plcpnl0086.prod.iad2.secureserver.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'webinfo@sachtechsolution.com';
        $mail->Password = 'webinfo';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->setFrom('webinfo@sachtechsolution.com', 'Proposal');   // sender
        $mail->addAddress("Asija72@gmail.com");
        $mail->addAddress("kapillikes@gmail.com");
        $mail->addAddress("sahildhawan1994@gmail.com");
        $mail->addAddress("harwinderkumar820@gmail.com");
        //$mail->addCC('sbsunilbhatia9@gmail.com');    //to add cc
        $mail->addAttachment('');         // Add attachments
        $mail->addAttachment('');    // Optional name
        $mail->isHTML(true);
        $mail->Subject = "Proposal'";
        $mail->Body = "<div style='width:100%;background:#ccc;padding-bottom:36px'><div style='width:80%;margin: 0 10%'>
        <div style='background:#eee;border-bottom:1px solid #ccc;height:63px;padding:10px 0 0 10px'><img style='float:left;
        height:50px' src='http://sachtechsolution.com/wp-content/uploads/2015/08/safdsgf.png' alt='SachTech Solution' />
        <p style='float: left; color: rgb(102, 102, 102); font-size: 15px; font-weight: bold; margin-left: 10px; margin-top: 26px;'>Proposal</p>
        </div><div style='background:#fff;margin-top:-15px;padding-left:69px;padding-bottom:35px'>
        <p style='color:green;line-height:40px;font-weight:bold;font-size:15px'>Greetings of the Day,</p>
        <table><tbody><tr><td style=\"padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%\">
        Full Name</td><td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $client_name . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Email</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'><a href=''>" . $client_email . "</a></td>
        </tr><tr><td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Phone</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $client_contact . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Budget</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $client_budget . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Country</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $client_country . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Skype/whats app</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $client_social_id . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Description</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $client_description . "</td></tr></tbody></table>
        <div style='background:#eee;border:1px solid #ccc;height:63px;padding:0 0 10px 10px'>
        <p style='font-weight:bold;color:#666'>Thanks & Regards<br><br>Sachtech Solution Team</p>
        </div></div></div>";
            if (!$mail->send()) {
                echo "Server Error";
            } else {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Mail Send Successfully";
            }

        return $this->response;
    }
    public function careerApplyMail($car_app_name,$car_app_email,$car_app_phone,$car_app_job_type,$file_url,$file_name)
    {
        $file_to_attach = $file_url;
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'a2plcpnl0086.prod.iad2.secureserver.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'webinfo@sachtechsolution.com';
        $mail->Password = 'webinfo';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->setFrom('webinfo@sachtechsolution.com', 'Proposal');   // sender
        /*$mail->addAddress("Asija72@gmail.com");
        $mail->addAddress("kapillikes@gmail.com");*/
        $mail->addAddress("diwanshu.tangri@sachtechsolution.com");
        $mail->addAddress("sahildhawan1994@gmail.com");
        $mail->addAddress("harwinderkumar820@gmail.com");
        //$mail->addCC('sbsunilbhatia9@gmail.com');    //to add cc
        $mail->addAttachment($file_to_attach,$file_name);         // Add attachments
        $mail->isHTML(true);
        $mail->Subject = "Career APPLY'";

        $mail->Body = "<div style='width:100%;background:#ccc;padding-bottom:36px'><div style='width:80%;margin: 0 10%'>
        <div style='background:#eee;border-bottom:1px solid #ccc;height:63px;padding:10px 0 0 10px'><img style='float:left;
        height:50px' src='http://sachtechsolution.com/wp-content/uploads/2015/08/safdsgf.png' alt='SachTech Solution' />
        <p style='float: left; color: rgb(102, 102, 102); font-size: 15px; font-weight: bold; margin-left: 10px; margin-top: 26px;'>New Proposal</p>
        </div><div style='background:#fff;margin-top:-15px;padding-left:69px;padding-bottom:35px'>
        <p style='color:green;line-height:40px;font-weight:bold;font-size:15px'>Greetings of the Day,</p>
        <table><tbody><tr><td style=\"padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%\">
        Full Name</td><td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $car_app_name . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Email</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'><a href=''>" . $car_app_email . "</a></td>
        </tr><tr><td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Phone</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $car_app_phone . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Job Type</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $car_app_job_type . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>File</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'><a href='".$file_to_attach."'> Download Resume</a></td></tr></tbody></table>
        
        <div style='background:#eee;border:1px solid #ccc;height:63px;padding:0 0 10px 10px'>
        <p style='font-weight:bold;color:#666'>Thanks & Regards<br><br>Sachtech Solution Team</p>
        </div></div></div>";
            if (!$mail->send()) {
                echo "Server Error";
            } else {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Mail Send Successfully";
            }

        return $this->response;
    }
    public function contactUsMail($contact_name,$contact_email,$contact_number,$contact_message)
    {
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'a2plcpnl0086.prod.iad2.secureserver.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'webinfo@sachtechsolution.com';
        $mail->Password = 'webinfo';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->setFrom('webinfo@sachtechsolution.com', 'Contact Mail');   // sender

        /*$mail->addAddress("Asija72@gmail.com");
        $mail->addAddress("kapillikes@gmail.com");*/
        $mail->addAddress("sahildhawan1994@gmail.com");
        $mail->addAddress("harwinderkumar820@gmail.com");
        $mail->addAddress("nehagarg.sachtech@gmail.com");
        $mail->addAddress("diwanshu.tangri@sachtechsolution.com");
        //$mail->addCC('sbsunilbhatia9@gmail.com');    //to add cc
        $mail->isHTML(true);
        $mail->Subject = "Contact us Mail'";

        $mail->Body = "<div style='width:100%;background:#ccc;padding-bottom:36px'><div style='width:80%;margin: 0 10%'>
        <div style='background:#eee;border-bottom:1px solid #ccc;height:63px;padding:10px 0 0 10px'><img style='float:left;
        height:50px' src='http://sachtechsolution.com/wp-content/uploads/2015/08/safdsgf.png' alt='Sachtech Solution' />
        </div><div align='center' style='background:#fff;margin-top:-15px;padding-bottom:35px;padding-left: 5px;padding-right: 5px'>
        <p style='color:green;line-height:40px;font-weight:bold;font-size:15px'>New Contact Us</p>
        <table><tbody><tr><td style=\"padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%\">
        Full Name</td><td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $contact_name . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Email</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'><a href=''>" . $contact_email . "</a></td>
        </tr><tr><td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Phone</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $contact_number . "</td></tr><tr>
        <td style='padding:8px;border:1px solid rgb(153,153,153);font-weight:bold;width:55%'>Contact Message</td>
        <td style='padding:8px;border:1px solid rgb(153,153,153);width:55%'>" . $contact_message . "</td></tr></tbody></table>
        <div style='background:#eee;border:1px solid #ccc;height:63px;padding:0 0 10px 10px'>
        <p style='font-weight:bold;color:#666'>Thanks & Regards<br><br>Sachtech Solution Team</p>
        </div></div></div>";
            if (!$mail->send()) {
                echo "Server Error";
            } else {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Mail Send Successfully";
            }

        return $this->response;
    }

    public function registerUser($username, $email, $contactNumber, $address, $registersource, $file_name)
    {
        $link = $this->link->connect();
        if ($link) {
            $emailResponse = $this->checkEmailExistence($email);
            if ($emailResponse[STATUS] == Error) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $emailResponse[MESSAGE];
                $this->response['UserData'] = $emailResponse['UserData'];
            } else {
                $token = $this->generateToken();
                $query = "insert into users (user_name,user_email,user_contact,user_status,user_token,user_address,
                register_source,email_verified,user_profile) VALUES ('$username','$email','$contactNumber','1','$token',
                '$address','$registersource','No','$file_name')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $last_id = mysqli_insert_id($link);
                    $userResponse = $this->getParticularUserData($last_id);
                    $UserData = $userResponse['UserData'];
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Verification Link has been Sent to your Registered E-Mail Address ";
                    $this->response['UserData'] = $UserData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function paymentUser($firstname,$lastname,$courses,$city,$address,$state,$country,$zip,$phone,$email,$amount)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "insert into student_form(firstname,lastname,courses,city,address,state,country,zip,phone,email,amount)
            VALUES ('$firstname','$lastname','$courses','$city','$address','$state','$country','$zip','$phone','$email','$amount')";
            $result = mysqli_query($link, $query);
            if($result){
                $last_id = mysqli_insert_id($link);
                $_SESSION['sach_userId'] = $last_id;
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "User registered successfully";
            }
            else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }

        }
        else {
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->link->response;
    }

    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}