<?php include("header.php"); ?>
<!--sachtech about us heading start here-->
<div class="col-md-12 nogutter">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <!-- about us part start here -->
        <div class="row about-us-main">
            <h1 class="about-us-content">
                ABOUT US
            </h1>
            <p class="about-sub-heading">
                Our Aim Is To Deliver The Services Above And Beyond Customer Expectations That Can Expand Their
                Boundaries With Our Innovative & Flawless Business Partnership With Them.
            </p>
        </div>
        <!-- about us part end here -->
    </div>
    <div class="col-md-1"></div>
</div>

<div class="clear"></div>
<!--sachtech about us heading start here-->
<div class="col-md-12 aboutus-menu-box">
    <div class="col-md-1"></div>
    <div class="col-md-10 aboutus-menu-box2">
        <div class="row aboutus-menu-ul-box">
            <ul class="about-menu-ul">
                <!--<a href="#foundation-box-content" class="about-menu-ancor">-->
                <li class="about-menu-li about-menu-li-active" id="foundation-box">
                    <div class="abt-social-icon">
                        <img src="images/construction-worker_white.png"
                             class="img-responsive abt-social-img img_nothoverd"
                             alt="ico35">
                        <img src="images/construction-worker_red.png" class="img-responsive abt-social-img img_nothov"
                             alt="ico35">
                    </div>
                    <!--<img src="images/ico36_hover.png" class="img-responsive hov_img" alt="ico35_hover">-->
                    <br>
                    <h4>FOUNDATION</h4>
                </li>
                <!-- </a>-->
                <li class="about-menu-li" id="vision-box">

                    <div class="abt-social-icon">
                        <img src="images/binocular_white.png" class="img-responsive abt-social-img img_nothoverd"
                             alt="ico35">
                        <img src="images/binocular_red.png" class="img-responsive abt-social-img img_nothov"
                             alt="ico35">
                    </div>
                    <br>
                    <h4>VISION</h4>
                </li>
                <li class="about-menu-li " id="mission-box">
                    <div class="abt-social-icon">
                        <img src="images/target_white.png" class="img-responsive abt-social-img img_nothoverd"
                             alt="ico35">
                        <img src="images/target_red.png" class="img-responsive abt-social-img img_nothov"
                             alt="ico35">
                    </div>
                    <!--<img src="images/ico36_hover.png" class="img-responsive hov_img" alt="ico35_hover">-->
                    <br>
                    <h4 class="li_ancor">MISSION</h4>
                </li>
                <li class="about-menu-li" id="value-box">
                    <div class="abt-social-icon">
                        <img src="images/sprout_white.png" class="img-responsive abt-social-img img_nothoverd"
                             alt="ico35">
                        <img src="images/sprout_red.png" class="img-responsive abt-social-img img_nothov"
                             alt="ico35">
                    </div>
                    <!--<img src="images/ico36_hover.png" class="img-responsive hov_img" alt="ico35_hover">-->
                    <br>
                    <h4>CORE VALUES</h4>
                </li>

            </ul>
        </div>
        <div class="clear"></div>
        <!----------------------------------------------------------------------------------------->
        <!-- About us header end here-->
        <!----------------------------------------------------------------------------------------->
    </div>
    <div class="col-md-1"></div>
</div>
<!----------------------------------------------------------------------------------------->
<!-- About us content start here-->
<!----------------------------------------------------------------------------------------->
<div class="col-md-12 abt-content-main12">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row abt-content-box" id="foundation-box-content">
            <div class="col-md-7 abt-content-inner-box">
                <div class="row about-detail-text">
                    <h4 class="abt-heading">
                        Our Foundation Is To Provide Spectrum Of Services That Improves Every Client's Business.
                    </h4>
                    <p class="abt-description">
                        SachTech Solution established back in 29 December, 2011 at Mohali, India to serve the varying
                        need of individuals as well as SMEs in today’s competitive market across the globe
                        It was incorporated as SACHTECH SOLUTION PRIVATE LIMITED with CIN U72900CH2016PTC041177 on 11th
                        Aug 2016 under the Companies Act, 2013 in India.
                        As of July 2017, SachTech has a strong team in Mohali, India lead by passionate young
                        entrepreneurs serving customers from across
                        the globe in following countries: USA, Canada, UK, Brazil, Spain, Malaysia, UAE, Egypt,
                        Australia, Finland and so on. We are continuously increasing our
                        reach with potential customers and determined to expand our services to everyone in the globe.
                        We embrace our responsibility to create a positive
                        impact in the communities in which we work and live.
                    </p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row abt-img-row">
                    <img class="img-responsive abt-goal-imgages foundation_image" src="images/foudationnnnnn.png"/>
                </div>
            </div>
        </div>
        <hr>
        <div class="row abt-content-box page_row_box" id="vision-box-content">
            <div class="col-md-5 about-img-right-box abt-content-img-box-right page_ist_div">
                <div class="row abt-img-row">
                    <img class="img-responsive abt-goal-imgages" src="images/shutterstock_633761795.jpg"/>
                    <!--<div class="abt-img-overlay">
                        <div class="abt-img-text">Our Vision</div>
                    </div>-->
                </div>
            </div>
            <div class="col-md-7 abt-content-inner-box abt-content-box-right page_2nd_div">
                <div class="row about-detail-text">
                    <h4 class="abt-heading">
                        Vision: COLLABORATIVE SPIRIT
                    </h4>
                    <p class="abt-description">
                        we are looking for our services reaches to iso standard and we strive to
                        our initiative reaches to 6 sigma level in quality management and services. we
                        want our service contributes to sustainable development for society.
                    </p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row abt-content-box" id="mission-box-content">
            <div class="col-md-7 abt-content-inner-box">
                <div class="row about-detail-text">
                    <h4 class="abt-heading">
                        Mission: To help businesses and societies
                    </h4>
                    <p class="abt-description">
                        Design and achieve market-leading performance roadmaps by combining creative thinking,
                        technology expertise and global reach.
                        To innovate in a way that minimizes the gap between our offerings and client needs, until both
                        are at zero distance from one another.
                        For us, innovation is a quest for a client problem that is as yet unarticulated, or finding that
                        surprisingly innovative way to execute
                        a solution to a known problem. Delivering on our commitment to make our business, our client's businesses
                        and our ecosystem sustainable across the three dimensions of economic, social and environmental development.
                    </p>
                </div>
            </div>
            <div class="col-md-5 ">
                <div class="row abt-img-row">
                    <img class="img-responsive abt-goal-imgages" src="images/shutterstock_550769707.jpg"/>
                </div>
            </div>
        </div>
        <hr>
        <div class="row abt-content-box page_row_box" id="value-box-content">
            <div class="col-md-5 abt-img-row abt-content-img-box-right page_ist_div">
                <div class="row">
                    <img class="img-responsive abt-goal-imgages" src="images/shutterstock_523305358.jpg"/>

                </div>
            </div>
            <div class="col-md-7 abt-content-inner-box abt-content-box-right page_2nd_div">
                <div class="row about-detail-text">
                    <h4 class="abt-heading">
                        Our Core Values: Integrity, Growth, Excellence, Service
                    </h4>

                    <h4 class="abt-sub-heading">
                        COLLABORATIVE SPIRIT:
                    </h4>
                    <p class="abt-sub-description">
                    <ul>
                        <li>
                            SachTech Solution believes in developing true partnerships. We foster a collegial
                            environment where individual perspectives are respected and honest dialogue is expected.
                        </li>
                    </ul>
                    </p>
                    <h4 class="abt-sub-heading">
                        UNRELENTING DEDICATION:
                    </h4>
                    <p class="abt-sub-description">
                    <ul>
                        <li>
                            SachTech Solution is driven to meet client needs with determination and grit. We embrace
                            tough challenges and do not rest until the problem is solved, the right way.
                        </li>
                    </ul>
                    </p>
                    <h4 class="abt-sub-heading">
                        EXPERT THINKING:
                    </h4>
                    <p class="abt-sub-description">
                        <ul>
                            <li>
                                SachTech Solution brings robust skills and forward looking perspectives to solve customer
                                challenges. We use proven knowledge to make recommendations and provide expert guidance to
                                our customers.
                            </li>
                        </ul>
                    </p>

                    <p class="abt-sub-description">
                        <ul>
                            <li>
                                We listen, we care, we serve.
                            </li>
                        </ul>
                    </p>

                    <p class="abt-sub-description">
                        <ul>
                            <li>
                                SachTech Solution innovate and constantly improve.
                            </li>
                        </ul>
                    </p>
                    <p class="abt-sub-description">
                    <ul>
                        <li>
                            We do what we say we’ll do.
                        </li>
                    </ul>
                    </p>
                    <p class="abt-sub-description">
                        <ul>
                            <li>
                                SachTech Solution believe in people and their dreams.
                            </li>
                        </ul>
                    </p>

                </div>
            </div>
        </div>

    </div>
    <div class="col-md-1"></div>
</div>
<!----------------------------------------------------------------------------------------->
<!-- About us content end here-->
<!----------------------------------------------------------------------------------------->


<?php include("footer.php"); ?>
<script>
    var url = window.location.href;
    if (url.indexOf("?") > 0) {
        url = url.split("?")[1];
        if (url == "mission") {
            $(".about-menu-li").removeClass("about-menu-li-active");
            $("#mission-box").addClass("about-menu-li-active");

            $('html, body').animate({
                scrollTop: $("#mission-box-content").offset().top
            }, 800);
        } else if (url == "foundation") {
            $(".about-menu-li").removeClass("about-menu-li-active");
            $("#foundation-box").addClass("about-menu-li-active");
            $('html, body').animate({
                scrollTop: $("#foundation-box-content").offset().top
            }, 800);
        } else if (url == "vision") {
            $(".about-menu-li").removeClass("about-menu-li-active");
            $("#vision-box").addClass("about-menu-li-active");
            $('html, body').animate({
                scrollTop: $("#vision-box-content").offset().top
            }, 800);
        } else if (url == "value") {
            $(".about-menu-li").removeClass("about-menu-li-active");
            $("#value-box").addClass("about-menu-li-active");
            $('html, body').animate({
                scrollTop: $("#value-box-content").offset().top
            }, 800);
        }
    }
</script>
