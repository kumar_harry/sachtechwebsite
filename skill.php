<?php include("header.php"); ?>
<div class="loader">
    <img src="images/159.gif" class="loader-img"/>
</div>
<!--skill slider start here-->
<div class="col-md-12 nogutter">
    <div class="row skill-top-banner">
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active" align="center">
                    <a class="serve-deve-bann-a" href="#">
                        <img src="images/skillslider/6.jpg" class=""/>
                    </a>
                </div>
                <div class="item" align="center">
                    <a class="serve-deve-bann-a" href="#">
                        <img src="images/skillslider/1.jpg" class=""/>
                    </a>
                </div>
                <div class="item" align="center">
                    <a class="serve-deve-bann-a" href="#">
                        <img src="images/skillslider/2.jpg" class=""/>
                    </a>
                </div>
                <div class="item" align="center">
                    <a class="serve-deve-bann-a" href="#">
                        <img src="images/skillslider/3.jpg" class=""/>
                    </a>
                </div>
                <div class="item" align="center">
                    <a class="serve-deve-bann-a" href="#">
                        <img src="images/skillslider/4.jpg" class=""/>
                    </a>
                </div>
                <div class="item" align="center">
                    <a class="serve-deve-bann-a" href="#">
                        <img src="images/skillslider/5.jpg" class=""/>
                    </a>
                </div>
            </div>
            <ol class="carousel-indicators">
                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example" data-slide-to="1"></li>
                <li data-target="#carousel-example" data-slide-to="2"></li>
                <li data-target="#carousel-example" data-slide-to="3"></li>
                <li data-target="#carousel-example" data-slide-to="4"></li>
                <li data-target="#carousel-example" data-slide-to="5"></li>
            </ol>
            <a class="left carousel-control" href="#carousel-example" role="button" data-slide="prev">
                <span class="fa fa-chevron-left fa-2x skill-carousal-arrow" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example" role="button" data-slide="next">
                <span class="fa fa-chevron-right fa-2x skill-carousal-arrow" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
<div class="clear"></div>
<!--skill slider end here-->
<!--skill body content start here-->
<div class="col-md-12 skill-content-main12">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row skill-content-box " id="foundation-box-content">
            <a id="section-motive"></a>
            <div class="col-md-7">
                <div class="row">
                    <h4 class="skill-heading">
                        Motive
                    </h4>
                    <p class="skill-description">
                        Main Motive of SachTech Skill development Programs is to take the Nation forward
                        digitally and Economically. If we consider the current generation of our life cycle,
                        our youth is the most populating generation. This generation basically believes in
                        practical approach, they want prosperity in the
                        industry; they don’t want to believe in the past experience they only believe in
                        the present. But what matter is they don’t know how to choose a right way for
                        bright career & not only engineering students, the aspirating students who want
                        to achieve numerous career objectives are also getting disturbed just because of
                        the obstacles which stops their path. So, now if you are one of those students you can
                        opt for SachTech skill development programs which will give you a way towards your goal.
                        Here we are briefing you about the various SachTech skill development programs so that
                        you will be clearer about your aim. SachTech Skill development programs are those
                        programs which include summer training programs, winter training programs with different
                        courses according to your choice for example ANDROID summer /winter training program,
                        IOT (internet on things) summer/winter training program, PHP summer/winter training
                        program, and NETWORKING summer/winter training program and JAVA and so..on. Now this is
                        interesting, each and every course is available in summers as well as in winters.
                    </p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row skill-img-row">
                    <img class="img-responsive skill-goal-imgages skill_motive" src="images/motive.jpg"/>
                </div>
            </div>
        </div>
        <div class="row skill-content-box page_row_box" id="vision-box-content">
            <a id="section-need"></a>
            <div class="col-md-5 page_ist_div">
                <div class="row skill-img-row">
                    <img class="img-responsive skill-goal-imgages" src="images/need.jpg"/>
                </div>
            </div>
            <div class="col-md-7 page_2nd_div">
                <div class="row">
                    <h4 class="skill-heading">
                        Need
                    </h4>
                    <p class="skill-description">
                        <span style="color:#ab1522">Experience : </span> Students that have acquire more well-rounded experiences and also get to see first-hand how
                        companies work. This provides a valuable understanding of the business and industry in which you
                        want to work in once you graduate.
                        <br>
                        <span style="color:#ab1522">Resume Builder : </span> Make sure to find skill development that are relevant to your major and add them to your resume.
                        This provides a great foundation in your desired industry and will propel you above other
                        college graduates with no skill development experience.
                        <br>
                        <span style="color:#ab1522">Gain Confidence : </span> You may not have the experience yet, or even the confidence to do the job you think you want,
                        however, most skill development train college students on-the-job and provide a fantastic
                        learning experience for them to build their confidence. This confidence shows, especially if you
                        are in the middle of a phone or in-person interview.
                    </p>
                </div>
            </div>
        </div>

        <!-- question answer part start here-->
        <div class="row skill-ques-box">
            <a id="section-stand-out"></a>
            <div class="container-fluid nogutter">
                <h4 class="skill-question">
                    HOW WE STAND OUT FROM OTHERS?
                </h4>
                <div class="skill-ques-ans">
                    <ul class="skill-ques-ans-ul">
                        <li class="skill-ques-ans-li">
                            We understand your concern and that is why we are always ready to clear your doubts in all
                            possible ways.
                        </li>
                        <li class="skill-ques-ans-li">
                            We train our trainees in all possible ways to maximize the value of their technology
                            investments by providing the best service.
                        </li>
                        <li class="skill-ques-ans-li">
                            With the rise in the competition where every other person stands above the other, it has
                            become important to beat the crowd in respect of skills and knowledge to ensure the success.
                            It is not easy to do this unless you challenge yourself and train yourself very hard and
                            ultimately the point is How to do it. You always need the help of experts who can train you
                            in every way to make you an expert and we at SachTech Solution are always ready to answer
                            all your questions and to give you the best skill development services.
                        </li>
                        <!--<li class="skill-ques-ans-li">
                            <div class="skill-li-content">
                                <div class="skill-ans-text skill-ans-textImg">
                                    We have a strong team of 40+ android app developers, iOS app developer and
                                    windows
                                    app developer who are well-versed to build apps.
                                </div>

                                <div class="skill-ans-img  skill-ans-imgText">
                                    <img src="images/shutterstock_433408534.jpg" class="img-responsive"/>
                                </div>
                            </div>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="row skill-ques-box ">
            <a id="section-program-offer"></a>
            <div class="container-fluid nogutter">
                <h4 class="skill-question">
                    How does the program we offer good for you?
                </h4>
                <div class="skill-ques-ans">
                    <ul class="skill-ques-ans-ul">
                        <li class="skill-ques-ans-li">
                            You can convert your academic knowledge into industry skills.
                        </li>
                        <li class="skill-ques-ans-li">
                            Gain exposure to real-world problems and issues that perhaps are not found in textbooks.
                        </li>
                        <li class="skill-ques-ans-li">
                            The experience will narrow down your list of potential careers:
                            Skill development really are a win-win situation. They can help you decide
                            if a particular career or area is or isn’t for you, and narrow down the (often long)
                            list of careers that you are interested in to find one that you will be happy
                            in.
                        </li>
                        <li class="skill-ques-ans-li">
                            Have personal growth experiences and exposure to different job opportunities.
                        </li>
                        <li class="skill-ques-ans-li">
                            Have hands-on opportunities to work with equipment and technology that may not be available
                            on campus.
                        </li>
                        <li class="skill-ques-ans-li">
                            Increase opportunities within a company for faster advancement and growth.
                        </li>
                        <li class="skill-ques-ans-li">
                            Increase self-confidence in the workplace while developing an expanded network of
                            associates and professionals.
                        </li>
                        <li class="skill-ques-ans-li">
                            You can make industry contacts:
                            It has been said many times : it’s not about what you know; it’s about who you know’.
                            While it is vital to know how to perform the required tasks for a particular job,
                            having a good set of industry contacts behind you can be just as vital in helping you
                            find and secure a job after you graduate.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- question answer part end   here-->
        <hr>
        <div class="row skill-stu-testimonial-row">
            <a id="section-student-testimonial"></a>
            <h4 class="skill-stud-test-heading">
                Student Testimonial
            </h4>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div id="student-testimonial" class="carousel slide student-testimonial" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active" align="center">
                            <a class="skill-stud-test-a" href="#">
                                <div class="skill-stud-test-img">
                                    <img src="images/userDefault.png"
                                         class="skill-testimonial-img img-circle"/>

                                </div>
                                <div class="skill-stud-test-box">
                                    <p class="skill-stu-test-name">Nitish</p>
                                    <p class="skill-stu-comment">
                                        RV sir best network administrator... lots of learning....confidence level is
                                        very high... best place for fresher..i like the most is company
                                        environment...thank you so much sir
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="item" align="center">
                            <a class="skill-stud-test-a" href="#">
                                <div class="skill-stud-test-img">
                                    <img src="images/userDefault.png"
                                         class="skill-testimonial-img img-circle"/>
                                </div>
                                <div class="skill-stud-test-box">
                                    <p class="skill-stu-test-name">Tinku Goel</p>
                                    <p class="skill-stu-comment">
                                        It is a good place for learning about computer applications. This company has
                                        the best environment for study. Every Student can enhance their skills at this
                                        company. I recommend this place to all students if they want to build a better
                                        future.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="item" align="center">
                            <a class="skill-stud-test-a" href="#">
                                <div class="skill-stud-test-img">
                                    <img src="images/userDefault.png"
                                         class="skill-testimonial-img img-circle"/>
                                </div>
                                <div class="skill-stud-test-box">
                                    <p class="skill-stu-test-name">Lucky Thakur</p>
                                    <p class="skill-stu-comment">
                                        SachTech Solution provides a perfect platform to kick start and boost your
                                        career goals. The company has productive environment and innovative project to
                                        work on. Best company for training and development
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="item" align="center">
                            <a class="skill-stud-test-a" href="#">
                                <div class="skill-stud-test-img">
                                    <img src="images/userDefault.png"
                                         class="skill-testimonial-img img-circle"/>
                                </div>
                                <div class="skill-stud-test-box">
                                    <p class="skill-stu-test-name">Parwinder singh</p>
                                    <p class="skill-stu-comment">
                                        SachTech Solution is a type of IT company which everyone is actually searching
                                        for having the start up of their career. I recommend everyone to be the part
                                        of SachTech Solution company for achieving their goals.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="item" align="center">
                            <a class="skill-stud-test-a" href="#">
                                <div class="skill-stud-test-img">
                                    <img src="images/userDefault.png"
                                         class="skill-testimonial-img img-circle"/>
                                </div>
                                <div class="skill-stud-test-box">
                                    <p class="skill-stu-test-name">Vandita Sehgal</p>
                                    <p class="skill-stu-comment">
                                        Best Company for 6 month industrial training. Highly skilled team of
                                        Professionals. I have done my 6 month industrial training in Android. There was
                                        a great facility to work with live projects.. Great Work
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <ol class="carousel-indicators student-testimonial-indicators">
                        <li data-target="#student-testimonial" data-slide-to="0" class="active"></li>
                        <li data-target="#student-testimonial" data-slide-to="1"></li>
                        <li data-target="#student-testimonial" data-slide-to="2"></li>
                        <li data-target="#student-testimonial" data-slide-to="3"></li>
                        <li data-target="#student-testimonial" data-slide-to="4"></li>
                    </ol>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <hr>
        <div class="row">
            <a id="section-technologies"></a>
            <h4 class="skill-question">Technologies</h4>
            <div class="col-md-12 skill_technologies">
                <div class="row skill-content-box page_row_box" id="vision-box-content">
                    <div class="col-md-5 page_ist_div">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages" src="images/skillslider/techImages/iot.png"/>
                        </div>
                    </div>
                    <div class="col-md-7 page_2nd_div">
                        <div class="row">
                            <a id="section-IOT"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> IOT (INTERNET OF THINGS)
                            </h4>
                            <p class="skill-description">
                                IOT means network of internet-connected objects able to collect and exchange data
                                using embedded sensors. Enable entities that utilize IoT devices to connect with
                                and control them using a dashboard, such as a mobile application. They include
                                smartphones, tablets, PCs, smartwatches, connected TVs, and nontraditional remotes.
                                SachTech Solution provides services to client with the combination of Mobile APP,
                                Embedded and complete web Solution. For ex: you are on your way to a meeting; your
                                car could have access to your calendar and already know the best route to take.
                                If the traffic is heavy your car might send a text to the other party notifying
                                them that you will be late. What if your alarm clock wakes up you at 6 a.m.
                                and then notifies your coffee maker to start brewing coffee for you? What if your
                                office equipment knew when it was running low on supplies and automatically
                                re-ordered more?<!--  What if the wearable device you used in the workplace could tell
                                    you when and where you were most active and productive and shared that information
                                    with other devices that you used while working?-->
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box " id="foundation-box-content">
                    <div class="col-md-8 ">
                        <div class="row">
                            <a id="section-ios"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-apple"></i> iOS
                            </h4>
                            <p class="skill-description">
                                iOS: Apple’s iPhone is considered to be one of the most popular smartphones in the
                                market today. The company has been successful in creating demand for its new
                                devices year after year consistently. It has not only been a trendsetter but
                                has revolutionized the smartphone industry. People around the world are aware of
                                how robust and easy-to-use iPhones are. In combination, the large pool of iPhone
                                apps available on the App store touch upon many aspects of our lives – be it
                                business or entertainment. Despite having 75% of SmartPhone market as Android users,
                                Apple still prevails when it comes to paying capacity of clients. Apple users are
                                more willing to pay on an app than android users. One of the main reasons behind
                                it is the open source heritage of Android. Moreover, the average cost of an
                                android app is $3.79, whereas for an iOS application, it’s $2.01, making a wide
                                base of paying clients.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages skill_technology"
                                 src="images/skillslider/techImages/apple.png"/>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box page_row_box" id="vision-box-content">
                    <div class="col-md-5 page_ist_div">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages"
                                 src="images/skillslider/techImages/java.png"/>
                        </div>
                    </div>
                    <div class="col-md-7 page_2nd_div">
                        <div class="row">
                            <a id="section-java"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> JAVA (JSE)
                            </h4>
                            <p class="skill-description">
                                Java: Java is a general-purpose computer programming language that is concurrent,
                                class-based, object-oriented, and specifically designed to have as few
                                implementation dependencies as possible. Java is a platform Independent: –
                                Platform independent means java can run on any computer irrespective to the
                                hardware and software dependency. Means Java does not depend on hardware means
                                what type of processor, RAM etc. Java will run on a machine which will satisfy
                                its basic needs. Java is fast because of JIT compiler. Just In Time compiler stores
                                the repeated code in its cache memory and in byte code where repeated code is used,
                                instead of loading that code again from memory JIT use it from its cache memory and
                                safe time and space and make execution fast.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box " id="foundation-box-content">
                    <div class="col-md-8 ">
                        <div class="row">
                            <a id="section-advance-java"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> ADVANCE JAVA (JEE)
                            </h4>
                            <p class="skill-description">
                                Advance Java: For Java and JEE developer, there is still wider/ larger scope for
                                these technologies. If you are excellent in the most basic concepts of Java
                                (abstraction, polymorphism, inheritance, naming conventions etc. you would get it
                                easier to learn advanced Java technologies, which is an added advantage for Java
                                developers. it would be helpful to learn or to get into project works on Java
                                frameworks such as Struts, Spring, Hibernate etc Also, web based applications are
                                given major considerations nowadays. So don't ever lose a chance where you are
                                getting chances to closely work with UI frameworks. To learn advance java and
                                related technologies.<br>
                                Advance java is concept that uses core java somehow which is used in real world
                                application. Its is what a client uses to run application or to create it.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages skill_technology"
                                 src="images/skillslider/techImages/java.png"/>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box page_row_box" id="vision-box-content">
                    <div class="col-md-5 page_ist_div">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages" src="images/skillslider/techImages/php.png"/>
                        </div>
                    </div>
                    <div class="col-md-7 page_2nd_div">
                        <div class="row">
                            <a id="section-php"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> PHP
                            </h4>
                            <p class="skill-description">
                                PHP: The complete code of PHP is open and anyone can use it free of cost
                                thus. Many beginners ask me if Security would be a problem then, to them this
                                is the same thing as you or any developer doing your work alone in a room and in
                                an open environment watched by thousand others. PHP is truly platform independent
                                where developers can write their programs on a windows machine and run it on a
                                Linux machine. The power of Platform independence also does not come at a cost of
                                performance. PHP is the winner between PHP vs All Languages to end the Why PHP
                                debate. Learning a new language is always an advantage and to have PHP in your
                                toolbox it will always serve you a good advantage. And to add to it as I say
                                always Don’t Marry with any language fall in love with Technology.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box " id="foundation-box-content">
                    <div class="col-md-8 ">
                        <div class="row">
                            <a id="section-android"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> ANDROID
                            </h4>
                            <p class="skill-description">
                                Android: Android has become very popular which is an open-source, Linux-based
                                operating system mainly designed by Google for smart-phones and tablets. It is
                                designed in such ways that allows the developers and device manufacturers companies
                                to alter the software design according to their needs. Android scope is huge and
                                interesting. It is also challenging. Globally, there are still many unexplored
                                domains in the enterprise segment, whereas in the consumers' segment the scope is
                                more competitive than the former. For instance, we have got very less android apps
                                that connects truck drivers. The scope is huge and massive. We still need some more
                                apps that connects technicians, like mechanic, electricians, plumbers etc. Android
                                apps does not need many people to develop unlike many enterprise products. Any good
                                individual with top skills with Android SDK & API, can develop it.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages skill_technology"
                                 src="images/skillslider/techImages/androis.png"/>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box page_row_box" id="vision-box-content">
                    <div class="col-md-5 page_ist_div">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages skill_technology"
                                 src="images/skillslider/techImages/networking.png"/>
                        </div>
                    </div>
                    <div class="col-md-7 page_2nd_div">
                        <div class="row">
                            <a id="section-networking"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> NETWORKING
                            </h4>
                            <p class="skill-description">
                                Networking is ideal for IT technicians and entry-level engineers aspiring to advance
                                their career in the networking field. The training is best suited for Network
                                Specialists, Network Administrators, Network Engineers, System Engineers, Network
                                Support Specialists, Network Administrators, Network Consultants and System Integrator.
                                Systems Administrator is the superset of Server Administrator, Storage Administrator,
                                Database Administrator, Network Administrator, and Security Administrator. Do
                                specialization in Computer Networking by doing PG in IISc, IIT, NIT etc. After
                                completing PG you will get a get chance to work with networking giants like Cisco,
                                Orange, and Juniper etc. Here you can get chance to work in their R&D department.
                                Another way is you can enter into this field by obtaining vendor certifications like
                                CCNA, CCNP, CCIE (Cisco track) or JNCIA, JNCIP, JNCIE (Juniper track) etc. By following
                                this certification track you can be able to design, install, configure and troubleshoot
                                small to large sized network. This is also a good choice to enter into networking field.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box " id="foundation-box-content">
                    <div class="col-md-8 ">
                        <div class="row">
                            <a id="section-cloud-computing"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> CLOUD COMPUTING
                            </h4>
                            <p class="skill-description">
                                Cloud Computing: Cloud Technologies are mostly virtualization environments.
                                Aspirants seeking to make good in the Cloud need to be adept in networking and
                                virtualization and gain hands-on exposure with live deployments. Candidates can
                                also experiment with their Cloud Technology Skills on few websites that offer lab
                                infrastructure such as Azure, AWS and Google Cloud among others. Cloud computing
                                is one of the most talked about IT trends today. The word ‘Cloud’ refers to the
                                widespread internet, which means Cloud Computing is an internet based computing
                                where services are delivered to the users via internet. There is no specific
                                qualification eligibility to learn Cloud Computing. The candidate should preferably
                                be from an IT or computer related background so that he/she has the general
                                knowledge about computers and programming. Knowledge of cloud computing basics or
                                any relevant experience could be a great add on to get a job. These could be
                                related to management, IT systems, end user support, application development,
                                business analysis, network, security and web development.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages skill_technology"
                                 src="images/skillslider/techImages/cloud.png"/>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box page_row_box" id="vision-box-content">
                    <div class="col-md-5 page_ist_div">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages" src="images/skillslider/techImages/hr.png"/>
                        </div>
                    </div>
                    <div class="col-md-7 page_2nd_div">
                        <div class="row">
                            <a id="section-hr"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> H-R (HUMAN RESOURCE)
                            </h4>
                            <p class="skill-description">
                                Human resources are the people who make up the workforce of an organization, business
                                sector, or economy. "Human capital" is sometimes used synonymously with "human
                                resources", although human capital typically refers to a more narrow view (i.e., the
                                knowledge the individuals embody and economic growth). Likewise, other terms sometimes
                                used include "manpower", "talent", "labour", "personnel", or simply "people". In
                                SachTech Solution human-resources department (HR department) of performs human resource
                                management, overseeing various aspects of employment, such as compliance with labour law
                                and employment standards, administration of employee benefits, and some aspects of
                                recruitment and dismissal. Also to determine needs of the staff, determine to use
                                temporary staff or hire employees to fill these needs, recruit and train the best
                                employees, supervise the work, manage employee relations, unions and collective
                                bargaining, prepare employee records and personal policies, ensure high performance,
                                manage employee payroll, benefits and compensation, ensure equal opportunities, deal
                                with discrimination, deal with performance issues, ensure that human resources practices
                                conform to various regulations, push the employees' motivation, managers need to develop
                                their interpersonal skills to be effective. Organisations behaviour focuses on how to
                                improve factors that make organisations more effective.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row skill-content-box " id="foundation-box-content">
                    <div class="col-md-8 ">
                        <div class="row">
                            <a id="section-digital-marketing"></a>
                            <h4 class="skill-heading">
                                <i class="fa fa-firefox"></i> DIGITAL MARKETING
                            </h4>
                            <p class="skill-description">
                                Digital marketing (also known as data-driven marketing) is an umbrella term for the
                                marketing of products or services using digital technologies, mainly on the Internet,
                                but also including mobile phones, display advertising, and any other digital medium. As
                                digital platforms are increasingly incorporated into marketing plans and everyday life,
                                and as people use digital devices instead of visiting physical shops, digital marketing
                                campaigns are becoming more prevalent and efficient.
                                SachTech Solution Digital marketing team is working on techniques such as search engine
                                optimization (SEO), search engine marketing (SEM), social media optimization (SMO),
                                content marketing, influencer marketing, content automation, campaign marketing,
                                data-driven marketing and e-commerce marketing, social media marketing, social media
                                optimization, e-mail direct marketing, display advertising, e–books, and optical disks
                                and games are becoming more common in our advancing technology.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="row skill-img-row">
                            <img class="img-responsive skill-goal-imgages skill_technology"
                                 src="images/skillslider/techImages/digital.png"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>

</div>
<!--skill body content end   here-->
<?php include("footer.php"); ?>
<script>
    var url = window.location.href;
    if (url.indexOf("?") > 0) {
        url = url.split("?")[1];
        if (url == "section-motive") {
            $('html, body').animate({
                scrollTop: $("#section-motive").offset().top
            }, 800);
        }
        else if (url == "section-need") {
            $('html, body').animate({
                scrollTop: $("#section-need").offset().top
            }, 800);
        }
        else if (url == "section-stand-out") {
            $('html, body').animate({
                scrollTop: $("#section-stand-out").offset().top
            }, 800);
        }
        else if (url == "section-program-offer") {
            $('html, body').animate({
                scrollTop: $("#section-program-offer").offset().top
            }, 800);
        }
        else if (url == "section-student-testimonial") {
            $('html, body').animate({
                scrollTop: $("#section-student-testimonial").offset().top
            }, 800);
        }
        else if (url == "section-ios") {
            $('html, body').animate({
                scrollTop: $("#section-ios").offset().top
            }, 800);
        }
        else if (url == "section-java") {
            $('html, body').animate({
                scrollTop: $("#section-java").offset().top
            }, 800);
        }
        else if (url == "section-advance-java") {
            $('html, body').animate({
                scrollTop: $("#section-advance-java").offset().top
            }, 800);
        }
        else if (url == "section-php") {
            $('html, body').animate({
                scrollTop: $("#section-php").offset().top
            }, 800);
        }
        else if (url == "section-android") {
            $('html, body').animate({
                scrollTop: $("#section-android").offset().top
            }, 800);
        }
        else if (url == "section-networking") {
            $('html, body').animate({
                scrollTop: $("#section-networking").offset().top
            }, 800);
        }
        else if (url == "section-cloud-computing") {
            $('html, body').animate({
                scrollTop: $("#section-cloud-computing").offset().top
            }, 800);
        }
        else if (url == "section-hr") {
            $('html, body').animate({
                scrollTop: $("#section-hr").offset().top
            }, 800);
        }
        else if (url == "section-digital-marketing") {
            $('html, body').animate({
                scrollTop: $("#section-digital-marketing").offset().top
            }, 800);
        }
        else if (url == "section-IOT") {
            $('html, body').animate({
                scrollTop: $("#section-IOT").offset().top
            }, 800);
        }
    }
</script>
